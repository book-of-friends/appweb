import React, {useContext, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {HeaderLoggedUserLinks} from "./HeaderUserLogged.jsx";
import {Context} from "../App.jsx";
import "../css/Header.css";
import {HeaderConnectionLinks, HeaderLogoutLinks} from "./HeaderGuestUser.jsx";

const LOGO_URL = "/cover.png";

const Header = () => {
    const {token, admin, setToken, setAdmin} = useContext(Context);
    const navigate = useNavigate();
    const [isActive, setisActive] = useState(false);

    const handleLogout = () => {
        navigate("/");
        localStorage.removeItem("token");
        localStorage.removeItem("admin");
        setToken("");
        setAdmin(false);
    };

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <Link to="/">
                    <img width="100" height="100" src={LOGO_URL} alt="Logo"/>
                </Link>

                <a className={`navbar-burger burger ${isActive ? "is-active" : ""}`}
                   onClick={() => {
                       setisActive(!isActive);
                   }}
                   role="button" aria-label="menu" aria-expanded="false"
                   data-target="navbarBasicExample"
                >
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" className={`navbar-menu ${isActive ? "is-active" : ""}`}>
                <div className="navbar-end">
                    {token ? (
                        <HeaderLoggedUserLinks admin={admin}/>
                    ) : (
                        <HeaderConnectionLinks/>
                    )}

                    {token && (
                        <HeaderLogoutLinks logout={handleLogout}/>
                    )}
                </div>
            </div>
        </nav>
    );
};

export {Header};
