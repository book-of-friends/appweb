import React from "react";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMessage, faUser} from "@fortawesome/free-solid-svg-icons";
import "../css/Header.css";

export const HeaderLoggedUserLinks = ({admin}) => {
    return (
        <div className="navbar-item">
            {!admin &&
                <div className="navbar-item">
                    <Link className="button is-light" to="/my-reservations">
                        My Reservations
                    </Link>
                </div>
            }
            <div className="navbar-item">
                <Link className="button is-light" to="/user-dashboard" title="Mon Profile">
                    <FontAwesomeIcon icon={faUser}/>
                </Link>
            </div>
            <div className="navbar-item">
                <Link className="button is-light" to="/commentaires" title="Commentaires">
                    <FontAwesomeIcon icon={faMessage}/>
                </Link>
            </div>
        </div>
    );
};
