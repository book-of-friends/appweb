import {Link} from "react-router-dom";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOut} from "@fortawesome/free-solid-svg-icons";

export const HeaderConnectionLinks = () => {
    return (
        <div className="navbar-item">
            <div className="navbar-item">
                <Link className="button is-light" to="/inscription">
                    <strong>Sign up</strong>
                </Link>
            </div>
            <div className="navbar-item">
                <Link className="button is-light" to="/connexion">
                    Log in
                </Link>
            </div>
        </div>
    );
};

export const HeaderLogoutLinks = ({logout}) => {
    return (
        <div className="navbar-item">
            <button className="button is-danger" onClick={logout} title="Log out">
                <FontAwesomeIcon icon={faSignOut}/>
            </button>
        </div>
    );
};

