import React, {useEffect, useState} from "react";
import "../css/login.css";
import {User} from ".././CommonFunction/user";
import {serveur} from "../CommonFunction/constantes.js";
import {VerificationCodePostal, VerificationInput, VerificationPhoneNumber} from "../Verification";

function UpdateAccountInformation() {
    const [message, setMessage] = useState("");
    const [isMessageVisible, setIsMessageVisible] = useState(false);
    const [personalInfo, setPersonalInfo] = useState({
        lastname: "", firstname: "", address: "",
        city: "", codePostal: "", country: "Canada",
        province: "Quebec", phone: "", dateOfBirth: ""
    });

    const formatInput = (name, value, formatFunction) => {
        const formattedValue = formatFunction(value);
        setPersonalInfo((prevData) => ({...prevData, [name]: formattedValue}));
    };

    const handleChange = (event) => {
        const {name, value} = event.target;

        if (name === "phone" || name === "codePostal") {
            formatInput(name, value, name === "phone" ? formatPhoneNumber : formatPostalCode);
        } else {
            setPersonalInfo((prevData) => ({...prevData, [name]: value}));
        }
    };

    const formatPhoneNumber = (phoneNumber) => {
        const formattedValue = phoneNumber.replace(/\D/g, "");
        const cleaned = ("" + formattedValue).replace(/\D/g, "");
        const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        return match ? `(${match[1]}) ${match[2]}-${match[3]}` : phoneNumber;
    };

    const formatPostalCode = (codePostal) => {
        const sanitizedValue = codePostal.replace(/[^a-zA-Z0-9]/g, "").toUpperCase();
        return sanitizedValue.replace(/^([a-zA-Z]\d[a-zA-Z])\s?(\d[a-zA-Z]\d)$/, "$1 $2");
    };

    const updatePersonalInfo = async () => {
        try {
            const response = await fetch(`${serveur}/user`, {
                method: "PUT",
                headers: {
                    authorization: `Bearer ${User.TOKEN}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(personalInfo),
            });

            const data = await response.json();
            setMessage(data.message);
            setIsMessageVisible(true);
        } catch (err) {
            alert(err);
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            const result = await new User().getPersonnalInfo();

            if (result.success) {
                setPersonalInfo({
                    ...result.data,
                });
            } else {
                setMessage(result.message);
                setIsMessageVisible(true);
            }
        };

        fetchData().then(() => console.log("User data fetched"));
    }, []);

    const renderInputField = (label, field, type = "text", disabled = false, verification) => (
        <div id="space">
            <label htmlFor={field}>
                <span>{label}</span>
            </label>
            <input
                type={type}
                id={field}
                name={field}
                size="60"
                maxLength="255"
                value={personalInfo[field]}
                className="form-control"
                aria-required="true"
                onChange={handleChange}
                disabled={disabled}
            />
            {verification}
        </div>
    );

    return (
        <div>
            {isMessageVisible && (
                <div className="modal is-active">
                    <div className="modal-background"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button className="delete" aria-label="delete"
                                        onClick={() => setIsMessageVisible(false)}></button>
                            </div>
                            <div className="message-body">
                                {message}
                            </div>
                        </article>
                    </div>
                </div>
            )}

            <fieldset className="column is-12-mobile is-8-desktop is-offset-2-tablet">
                <legend className="has-text-centered">
                    <span className="fieldset-legend">PERSONAL INFORMATION</span>
                </legend>
                <div className="fieldset-wrapper">
                    {renderInputField("First name", "firstname", "text", false, <VerificationInput
                        input={personalInfo.firstname}/>)}
                    {renderInputField("Last name", "lastname", "text", false, <VerificationInput
                        input={personalInfo.lastname}/>)}
                    {renderInputField("Date of birth", "dateOfBirth", "date", false, <VerificationInput
                        input={personalInfo.dateOfBirth}/>)}
                    {renderInputField("Residential address", "address", "text", false, <VerificationInput
                        input={personalInfo.address}/>)}
                    {renderInputField("City", "city", "text", false, <VerificationInput input={personalInfo.city}/>)}
                    {renderInputField("Postal Code", "codePostal", "text", false, <VerificationCodePostal
                        codePostal={personalInfo.codePostal}/>)}
                    {renderInputField("Country", "country", "text", true, <VerificationInput
                        input={personalInfo.country}/>)}
                    {renderInputField("Province", "province", "text", true, <VerificationInput
                        input={personalInfo.province}/>)}
                    {renderInputField("Phone", "phone", "text", false, <VerificationPhoneNumber
                        input={personalInfo.phone}/>)}
                    <br/>
                    <div className="columns is-mobile is-centered">
                        <button className="button-form" onClick={updatePersonalInfo}>
                            Submit
                        </button>
                    </div>
                </div>
                <br/>
                <br/>
            </fieldset>
        </div>
    );
}

export {UpdateAccountInformation};
