import React, {useEffect, useState} from "react";
import "../css/UserPage.css";
import {User} from ".././CommonFunction/user";
import {UpdatePassword} from "./UpdatePassword";
import {UpdateAccountInformation} from "./UpdateAccountInformation.jsx";
import {DeleteAccount} from "./DeleteAccount.jsx";
import {UpdateContactInformation} from "./UpdateContactInformation.jsx";

function UserDashboard() {
    const user = new User();

    const [firstname, setFirstname] = useState("");
    const [personalInfo, setPersonalInfo] = useState(true);
    const [contactInfo, setContactInfo] = useState(false);
    const [password, setPassword] = useState(false);
    const [deleteAccount, setDeleteAccount] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            const data = await user.getPersonnalInfo();
            setFirstname(data.data.firstname);
            console.log("User data fetched");
        };

        fetchData().then(() => console.log("User data fetched"));
    }, [user.getPersonnalInfo]);

    const handleMenuItemClick = (infoType) => {
        setPersonalInfo(infoType === "personal");
        setPassword(infoType === "password");
        setContactInfo(infoType === "contact");
        setDeleteAccount(infoType === "delete");
    };

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>{`${firstname}'s profile page`}</h1>
            </div>

            <div>
                <br/>
            </div>

            <div className="columns is-mobile">
                <div className="column is-3">
                    <div className="field" onClick={() => handleMenuItemClick("personal")}>
                        <span className="text is-size-5 is-clickable is-link is-underlined">
                            Update account information
                        </span>
                    </div>
                    <div className="field" onClick={() => handleMenuItemClick("contact")}>
                        <span className="text is-size-5 is-clickable is-link is-underlined">
                            Update contact information
                        </span>
                    </div>
                    <div className="field" onClick={() => handleMenuItemClick("password")}>
                        <span className="text is-size-5 is-clickable is-link is-underlined">
                            Update password
                        </span>
                    </div>
                    <div className="field" onClick={() => handleMenuItemClick("delete")}>
                        <span className="text is-size-5 is-clickable is-link is-underlined">
                            Delete account
                        </span>
                    </div>
                </div>

                <div className="column is-1">
                    <div className="column-separator"></div>
                </div>

                <div className="column">
                    {(() => {
                        switch (true) {
                            case personalInfo:
                                return <UpdateAccountInformation/>;
                            case password:
                                return <UpdatePassword/>;
                            case contactInfo:
                                return <UpdateContactInformation/>;
                            case deleteAccount:
                                return <DeleteAccount/>;
                            default:
                                return null;
                        }
                    })()}
                </div>
            </div>
        </main>
    );
}

export {UserDashboard};
