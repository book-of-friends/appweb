import React, {useState} from "react";
import "../css/login.css";
import {User} from ".././CommonFunction/user";
import {serveur} from "../CommonFunction/constantes.js";
import {VerificationConfirmation, VerificationPass} from "../Verification.jsx";

function UpdatePassword() {
    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confPassword, setConfPassword] = useState("");
    const [message, setMessage] = useState("");
    const [isMessageVisible, setIsMessageVisible] = useState(false);

    const handleChange = (field, value) => {
        switch (field) {
            case "password":
                setPassword(value);
                break;
            case "newPassword":
                setNewPassword(value);
                break;
            case "confPassword":
                setConfPassword(value);
                break;
            default:
                break;
        }
    };

    async function updatePassword({password, newPassword}) {
        try {
            const body = {
                password: password,
                newPassword: newPassword,
            };

            const response = await fetch(`${serveur}/user/update-password`, {
                method: "PUT",
                headers: {
                    authorization: `Bearer ${User.TOKEN}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body),
            });
            const data = await response.json();
            setMessage(data.message);
            setIsMessageVisible(true);
        } catch (err) {
            alert(err);
        }
    }


    const renderPasswordField = (label, field, verification) => (
        <div id="space">
            <label htmlFor={field}>
                <span>{label}</span>
            </label>
            <input
                className="form-control"
                type="password"
                id={field}
                name={field}
                size="60"
                maxLength="254"
                aria-required="true"
                onChange={(e) => handleChange(field, e.target.value)}
            />
            {verification}
        </div>
    );

    return (
        <div>
            {isMessageVisible && (
                <div className="modal is-active">
                    <div className="modal-background" aria-disabled="true"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button className="delete" aria-label="delete"
                                        onClick={() => setIsMessageVisible(false)}></button>
                            </div>
                            <div className="message-body">
                                {message}
                            </div>
                        </article>
                    </div>
                </div>
            )}

            <fieldset className="column is-12-mobile is-8-desktop is-offset-2-tablet">
                <legend className="has-text-centered">
                    <span className="fieldset-legend">PASSWORD MODIFICATION</span>
                </legend>
                <div className="fieldset-wrapper">
                    {renderPasswordField("Current Password", "password")}
                    {renderPasswordField("New Password", "newPassword", <VerificationPass password={newPassword}/>)}
                    {renderPasswordField("Confirm Password", "confPassword", <VerificationConfirmation
                        password={newPassword} confPassword={confPassword}/>)}
                    <br/>
                    <div className="columns is-mobile is-centered">
                        <button
                            className="button-form columns is-centered"
                            onClick={() => updatePassword({password, newPassword})}
                        >
                            Submit
                        </button>
                    </div>
                </div>
                <br/>
                <br/>
            </fieldset>
        </div>
    );
}

export {UpdatePassword};
