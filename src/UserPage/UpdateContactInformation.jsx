import React, {useEffect, useState} from "react";
import "../css/login.css";
import {User} from ".././CommonFunction/user";
import {serveur} from "../CommonFunction/constantes.js";

function UpdateContactInformation() {
    const [email, setEmail] = useState("");
    const [confEmail, setConfEmail] = useState("");
    const [isDisplayed, setIsDisplayed] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    const handleChangeEmail = (event) => setEmail(event.target.value);
    const handleChangeConfEmail = (event) => setConfEmail(event.target.value);

    useEffect(() => {
        const user = new User();
        user.getPersonnalInfo().then((data) => {
            setEmail(data.email);
        });
    }, []);

    const handleSubmitEmail = async () => {
        try {
            await updateCourriel({email});
            console.log("Email updated");
        } catch (err) {
            console.error(err);
            setErrorMessage("An error occurred. Please try again.");
            setIsDisplayed(true);
        }
    };

    async function updateCourriel({email}) {
        const body = {email: email};

        const response = await fetch(`${serveur}/user/update-email`, {
            method: "PUT",
            headers: {
                authorization: `Bearer ${User.TOKEN}`,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        });

        const data = await response.json();
        setErrorMessage(data.message);
        setIsDisplayed(true);
    }

    return (
        <div>
            {isDisplayed && (
                <div className="modal is-active">
                    <div className="modal-background" aria-disabled="true"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button className="delete" aria-label="delete"
                                        onClick={() => setIsDisplayed(false)}></button>
                            </div>
                            <div className="message-body">
                                {errorMessage}
                            </div>
                        </article>
                    </div>
                </div>
            )}

            <fieldset className="column is-12-mobile is-8-desktop is-offset-2-tablet">
                <legend className="has-text-centered">
                    <span className="fieldset-legend">EMAIL MODIFICATION</span>
                </legend>
                <div className="fieldset-wrapper">
                    <div id="space">
                        <label htmlFor="email">
                            <span>Email</span>
                        </label>
                        <input
                            className="form-control"
                            type="email"
                            id="email"
                            name="email"
                            value={email}
                            size="60"
                            maxLength="254"
                            aria-required="true"
                            onChange={handleChangeEmail}
                        />
                    </div>
                    <div id="space">
                        <label htmlFor="ConfEmail">
                            <span>Email Confirmation</span>
                        </label>
                        <input
                            className="form-control"
                            type="text"
                            id="ConfEmail"
                            name="ConfEmail"
                            size="60"
                            maxLength="254"
                            aria-required="true"
                            onChange={handleChangeConfEmail}
                        />

                        <span id="emailConfirmation">
                            {
                                email === confEmail && email !== "" ?
                                    <span className="icon is-small has-text-success">
                                        <i className="fas fa-check"></i>
                                    </span>
                                    :
                                    <span className="icon is-small has-text-danger">
                                        <i className="fas fa-times"></i>
                                    </span>
                            } the email and its confirmation have to be similar. <br/>
                        </span>
                    </div>
                    <br/>
                    <div className="columns is-mobile is-centered">
                        <button className="button-form" onClick={handleSubmitEmail}>
                            Submit
                        </button>
                    </div>
                </div>
                <br/>
                <br/>
            </fieldset>
        </div>
    );
}

export {UpdateContactInformation};
