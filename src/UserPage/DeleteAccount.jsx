import React, {useContext, useState} from "react";
import "../css/login.css";
import {User} from ".././CommonFunction/user";
import {Context} from "../App.jsx";
import {useNavigate} from "react-router-dom";
import {serveur} from "../CommonFunction/constantes.js";

function DeleteAccount() {
    const lecontext = useContext(Context);
    const navigate = useNavigate();
    const [isDisplayed, setIsDisplayed] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    const handleDeleteConfirmation = () => {
        setIsDisplayed(true);
    };

    const handleCancelDelete = () => {
        setIsDisplayed(false);
        setErrorMessage(""); // Reset error message on cancel
    };


    async function deleteAccount() {
        try {
            const response = await fetch(`${serveur}/user`, {
                method: "DELETE",
                headers: {authorization: `Bearer ${User.TOKEN}`},
            });
            const data = await response.json();

            if (response.ok) {
                localStorage.removeItem("token");
            } else {
                setErrorMessage(data.message);
                setIsDisplayed(true);
            }
        } catch (err) {
            alert(err);
        }
    }

    const handleConfirmDelete = async () => {
        await deleteAccount();
        navigate("/");
        lecontext.setToken("");
        lecontext.setAdmin(false);
    };

    return (

        <div>
            {isDisplayed && (
                <div className="modal is-active">
                    <div className="modal-background" aria-disabled="true"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button className="delete" aria-label="delete"
                                        onClick={() => setIsDisplayed(false)}></button>
                            </div>
                            <div className="message-body">
                                {errorMessage}
                            </div>
                        </article>
                    </div>
                </div>
            )}

            <fieldset className="column is-12-mobile is-8-desktop is-offset-2-tablet has-text-centered">
                <legend>
                    <span className="fieldset-legend">DELETE ACCOUNT</span>
                </legend>
                <div className="fieldset-wrapper columns is-centered">
                    <button className="button is-danger" onClick={handleDeleteConfirmation}>
                        DELETE
                    </button>

                    {isDisplayed && (
                        <div id="modal-ter" className="modal is-active">
                            <div className="modal-background" onClick={handleCancelDelete}></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">ATTENTION!!</p>
                                    <button className="delete" aria-label="close" onClick={handleCancelDelete}></button>
                                </header>
                                <section className="modal-card-body"> Do you want to delete your account?</section>
                                <div className="modal-card-foot">
                                    <button className="button is-danger" onClick={handleConfirmDelete}>
                                        YES
                                    </button>
                                    <button className="button" onClick={handleCancelDelete}>
                                        NO
                                    </button>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <br/>
                <br/>
            </fieldset>
        </div>
    );
}

export {DeleteAccount};
