import {Link} from "react-router-dom";


export function Footer() {
    return (
        <footer id="footer" className="footer">
            <div className="content has-text-centered">
                <p className="is-size-6 is-underlined">&copy;Book Of Friends {new Date().getFullYear()}</p>
            </div>

            <div className="columns is-mobile is-centered mt-4">
                <div className="column is-8-mobile is-9-tablet is-7-desktop mb-4">
                    <h4 className="title is-size-6 is-underlined has-text-weight-bold has-text-left">
                        Book Of Friends
                    </h4>

                    <p className="subtitle is-size-7 has-text-left">
                        Book of Friends provides free and open access to a vast collection of documents. As a public
                        library, it also offers numerous digital resources.
                    </p>
                </div>

                <div className="column is-2-desktop mb-4">
                    <h4 className="title is-size-6 is-underlined has-text-weight-bold has-text-justify">
                        Documents
                    </h4>

                    <p className="subtitle is-size-7 has-text-left">
                        <Link to="/albums">Albums</Link><br/>
                        <Link to="/books">Books</Link><br/>
                        <Link to="/movies">Movies</Link><br/>
                        <Link to="/products">View All</Link>
                    </p>
                </div>
            </div>
        </footer>
    );
}
