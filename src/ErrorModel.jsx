import React from "react";
import "./css/signup.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faExclamationCircle} from "@fortawesome/free-solid-svg-icons";

function ErrorModel({error, isErrorVisible}) {

    return (
        <div className={`form-element erreur ${isErrorVisible ? "" : "is-hidden"}`}>
            <h2>
                &nbsp;&nbsp;<FontAwesomeIcon icon={faExclamationCircle}/>
                &nbsp;Error in the form
            </h2>
            <ul>
                <li id="errorMessage">{error}</li>
            </ul>
        </div>
    );
}

export {ErrorModel};
