import "../css/signup.css";

function NoMatch() {
    return (
        <main className="section" id="Contenu">
            <div className="title is-1 has-text-centered form-control">
                <div>
                    <h1 className="title is-1 has-text-centered"> 404 PAGE NOT FOUND </h1>
                </div>
            </div>
        </main>
    );
}

export {NoMatch};
