import "../css/signup.css";

function NoAuth() {
    return (
        <main className="section" id="Contenu">
            <div className="title is-1 has-text-centered form-control">
                <div>
                    <h1 className="title is-1 has-text-centered"> 403 NOT AUTHORIZED </h1>
                </div>
            </div>
        </main>
    );
}

export {NoAuth};
