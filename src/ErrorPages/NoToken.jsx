import "../css/signup.css";

function NoToken() {
    return (
        <main className="section" id="Contenu">
            <div className="title is-1 has-text-centered form-control">
                <div>
                    <h1 className="title is-1 has-text-centered"> 401 NOT AUTHENTICATED </h1>
                </div>
            </div>
        </main>
    );
}

export {NoToken};
