import React, {useContext, useState} from "react";
import "../css/login.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCalendarDays} from "@fortawesome/free-solid-svg-icons";
import {Context} from "../App";
import {useNavigate} from "react-router-dom";
import {IS_MOVIE, serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";

function MovieDetails({movie}) {
    const [estAffiche, setEstAffiche] = useState(false);
    const [IdNum, setIdNum] = useState("");
    const context = useContext(Context);
    const navigate = useNavigate();

    const handleChangeIdNum = (e) => setIdNum(e.target.value);
    const {
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        showSuccess,
    } = useNotification();

    const handleReserveClick = () => {
        if (context.token) setEstAffiche(true);
        else {
            localStorage.setItem("path", window.location.pathname);
            navigate("/connexion");
        }
    };

    async function reserveArticle() {
        const state = movie.movieState.filter((e) => e.volume === IdNum)[0].availability;
        if (state === "Reserved") {
            setEstAffiche(false);
            showSuccess("This movie is already reserved.");
            return;
        } else if (state === "Borrowed") {
            setEstAffiche(false);
            showSuccess("This movie is already borrowed.");
            return;
        }

        try {
            const response = await fetch(`${serveur}/reservation?Id=${movie.id}&IdNum=${IdNum}&Type=${IS_MOVIE}`, {
                method: "POST",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
            });

            const data = await response.json();

            setEstAffiche(false);
            showSuccess(data.message);

        } catch (err) {
            console.error(err);
            showSuccess("An unexpected error occurred.");
        }
    }


    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>{movie.title}</h1>
            </div>

            <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                          setIsSuccessVisible={setIsSuccessVisible} page={`movie/${movie.id}`}/>

            <div>
                <br/>
            </div>
            <div className="columns is-mobile  is-multiline">
                <div className="column is-3-desktop is-12-mobile">
                    <img src={`${serveur}/cover/${movie.coverURL}`} alt={movie.title} style={{width: "100%"}}/>
                </div>
                <div className="column is-9-desktop is-8-mobile">
                    <div>
                        <br/>
                    </div>
                    <span className="subtitle is-5">
                        <strong>Title: </strong>{movie.title}<br/>
                        <strong>Author: </strong>{movie.author}<br/>
                        <strong>Language: </strong>{movie.language}<br/>
                        <strong>Country of Origin: </strong>{movie.country}<br/>
                        <strong>Document Type: </strong>{movie.type}<br/>
                        <strong>Rating: </strong>{movie.rating}<br/>
                        {
                            (movie.type !== "Série") ?
                                <span>
                                    <strong>Duration: </strong>{movie.duration}<br/>
                                </span> :
                                <span>
                                    <strong>Number of Episodes: </strong>{movie.duration}<br/>
                                </span>
                        }
                        <strong>Number of Seasons: </strong>{movie.seasons}<br/>
                        <strong>Genres: </strong>{movie.genres.map((e) => e.value).join(", ")} <br/>
                        <strong>Publication Date: </strong>{movie.releaseDate}<br/>
                        <strong>Actors: </strong>{movie.actors.map((e) => e.name).join(", ")}<br/>
                    </span>
                </div>
                <div className="column is-12">
                    <span className="subtitle is-5"><strong>Plot : </strong>{movie.synopsis}<br/></span>
                </div>
                <div className="column is-12">
                    <div style={{
                        overflow: "hidden",
                        padding: "0px",
                        borderBottom: "1px solid rgb(224, 224, 224)",
                        textAlign: "left",
                    }}>
                        <div style={{display: "inline-block",}}>
                            <div>
                                <span id="title">
                                    <h3><strong>Copies</strong></h3>
                                </span>
                            </div>
                        </div>
                        <div style={{float: "right",}}>
                            <button className="button" onClick={handleReserveClick}>
                                <div className="columns is-mobile is-multiline">
                                    <span className="column is-12">
                                        <FontAwesomeIcon icon={faCalendarDays}/><br/>
                                        Reserve
                                    </span>
                                </div>
                            </button>
                        </div>

                        <div className={estAffiche ? "modal is-active" : "modal"}>
                            <div className="modal-background"></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">Reservation Request</p>
                                    <button className="delete" aria-label="close" onClick={() => {
                                        setEstAffiche(false);
                                    }}></button>
                                </header>
                                <section className="modal-card-body">
                                    If this title has more than one season, please select the season you wish to
                                    reserve.
                                    <br/>
                                    <br/>
                                    {
                                        movie.movieState.map((e) =>
                                            <label className="subtitle is-5" key={e.volume} style={{height: "10px",}}>
                                                <span>
                                                    <span>
                                                        <input name="volume" type="radio" value={e.volume}
                                                               onChange={handleChangeIdNum}/>
                                                    </span>
                                                </span>
                                                &nbsp;&nbsp;
                                                <span>{e.volume}</span>
                                            </label>
                                        )
                                    }
                                    <br/>
                                </section>
                                <div className="modal-card-foot">
                                    <button className="button is-link is-light" onClick={reserveArticle}>CONFIRM
                                    </button>
                                    <button className="button" onClick={() => {
                                        setEstAffiche(false);
                                    }}>CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>

                        <ul>
                            {
                                movie.movieState.map(
                                    (e) =>
                                        <li key={e.volume}>
                                            <span className="subtitle is-5">
                                                {movie.title} - {e.volume} <br/>
                                                <u>{e.availability}</u>
                                            </span>
                                            <hr/>
                                        </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </main>
    );
}

export {MovieDetails};