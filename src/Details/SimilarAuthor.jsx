import React from "react";
import {Link} from "react-router-dom";
import "../css/login.css";
import {serveur} from "../CommonFunction/constantes.js";

function Similar({similar}) {
    return (
        <div className="column is-2-desktop is-4-mobile is-4-tablet is-clickable">
            <Link
                to={
                    similar.type === "Album"
                        ? `/album/${similar.id}`
                        : similar.type === "Manga" || similar.type === "Roman"
                            ? `/book/${similar.id}`
                            : `/movie/${similar.id}`
                }
                target="_parent"
            >
                <div>
                    <div className="card">
                        <div className="card-image">
                            <figure className="image">
                                <img alt={`Cover for ${similar.title}`} src={`${serveur}/cover/${similar.coverURL}`}/>
                            </figure>
                        </div>
                        <div className="card-content">
                            <div className="media">
                                <div className="media-content has-text-centered">
                  <span className="title is-7 is-small">
                    {similar.title}
                      <br/>
                      {similar.author}
                      <br/>
                      {similar.type}
                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    );
}

export {Similar};
