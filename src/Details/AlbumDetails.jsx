import React, {useContext, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCalendarDays} from "@fortawesome/free-solid-svg-icons";
import {Context} from "../App";
import {useNavigate} from "react-router-dom";
import "../css/login.css";
import {IS_ALBUM, serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";

function AlbumDetails({album}) {
    const [estAffiche, setEstAffiche] = useState(false);
    const [IdNum, setIdNum] = useState("");
    const lecontext = useContext(Context);
    const navigate = useNavigate();
    const isTokenAvailable = lecontext.token;

    const handleChangeIdNum = (e) => setIdNum(e.target.value);

    const {
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        showSuccess,
    } = useNotification();

    const handleReserveClick = () => {
        if (isTokenAvailable) setEstAffiche(true);
        else {
            localStorage.setItem("path", window.location.pathname);
            navigate("/connexion");
        }
    };


    async function reserveArticle() {
        if (album.albumState === "Reserved") {
            setEstAffiche(false);
            showSuccess("This album is already reserved.");
            return;
        } else if (album.albumState === "Borrowed") {
            setEstAffiche(false);
            showSuccess("This album is already borrowed.");
            return;
        }

        try {
            const response = await fetch(`${serveur}/reservation?Id=${album.id}&IdNum=${IdNum}&Type=${IS_ALBUM}`, {
                method: "POST",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
            });

            const data = await response.json();

            setEstAffiche(false);
            showSuccess(data.message);

        } catch (err) {
            console.error(err);
            showSuccess("An unexpected error occurred.");
        }
    }

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>{album.title}</h1>
            </div>

            <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                          setIsSuccessVisible={setIsSuccessVisible} page={`album/${album.id}`}/>

            <div>
                <br/>
            </div>

            <div className="columns is-mobile is-multiline">
                <div className="column is-3-desktop is-12-mobile">
                    <img src={`${serveur}/cover/${album.coverURL}`} alt={album.title} style={{width: "100%"}}/>
                </div>

                <div className="column is-9-desktop is-12-mobile">
                    <div>
                        <br/>
                    </div>
                    <span className="subtitle is-5">
                        <strong>Title: </strong>{album.title}<br/>
                        <strong>Author: </strong>{album.author}<br/>
                        <strong>Label: </strong>{album.label}<br/>
                        <strong>Language: </strong>{album.language}<br/>
                        <strong>Document Type: </strong>Album<br/>
                        <strong>Publication Date: </strong>{album.releaseDate}<br/>
                    </span>
                </div>

                <div className="column is-12">
                    <div className="columns is-mobile is-multiline">
                        <div className="column is-2">
                            <span className="subtitle is-5">
                                <strong>Tracks lists : </strong>
                            </span>
                        </div>
                        <div className="column is-10">
                            <span className="subtitle is-5 columns is-mobile is-multiline">
                                {album.tracks.map((e) =>
                                    <span className="column is-6" key={e.id}>
                                        <i className="fas fa-music"></i> {e.name}<br/>
                                    </span>
                                )}
                            </span>
                        </div>
                    </div>
                </div>

                <div className="column is-12">
                    <div style={{
                        overflow: "hidden",
                        padding: "0px",
                        borderBottom: "1px solid rgb(224, 224, 224)",
                        textAlign: "left"
                    }}>
                        <div style={{display: "inline-block"}}>
                            <div>
                                <span id="title">
                                    <h3><strong>Copies</strong></h3>
                                </span>
                            </div>
                        </div>
                        <div style={{float: "right"}}>
                            <button className="button" onClick={handleReserveClick}>
                                <div className="columns is-mobile is-multiline">
                                    <span className="column is-12">
                                        <FontAwesomeIcon icon={faCalendarDays}/>
                                        <br/>
                                        Reserve
                                    </span>
                                </div>
                            </button>
                        </div>

                        <div className={estAffiche ? "modal is-active" : "modal"}>
                            <div className="modal-background"></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">Reservation Request</p>
                                    <button className="delete" aria-label="close" onClick={() => {
                                        setEstAffiche(false);
                                    }}></button>
                                </header>
                                <section className="modal-card-body">
                                    If this title has more than one copy, please select the copy you want to reserve.
                                    <br/>
                                    <br/>
                                    <label className="subtitle is-5" key={album.title} style={{height: "10px",}}>
                                        <span>
                                            <span>
                                                <input name="title" type="radio" value={album.title}
                                                       onChange={handleChangeIdNum}/>
                                            </span>
                                        </span>
                                        &nbsp;&nbsp;
                                        <span>{album.title}</span>
                                    </label>
                                    <br/>
                                </section>
                                <div className="modal-card-foot">
                                    <button className="button is-link is-light"
                                            onClick={reserveArticle}>CONFIRM
                                    </button>
                                    <button className="button" onClick={() => {
                                        setEstAffiche(false);
                                    }}>CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>

                        <ul>
                            <li key={album.albumState}>
                                <span className="subtitle is-5">
                                    {album.title} - {album.author} <br/>
                                    <u>{album.albumState}</u>
                                </span>
                                <hr/>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </main>
    );
}

export {AlbumDetails};