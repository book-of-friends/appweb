import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import "../css/login.css";
import {AlbumDetails} from "./AlbumDetails";
import {BookDetails} from "./BookDetails";
import {MovieDetails} from "./MovieDetails";
import {Similar} from "./SimilarAuthor";
import {serveur} from "../CommonFunction/constantes.js";

function Details() {
    const {Id} = useParams();
    const [routeType] = useState(window.location.pathname.split("/")[1]);
    const [fetchData, setFetchData] = useState(null);
    const [sameAuthor, setSameAuthor] = useState([]);
    const [message, setMessage] = useState("");
    const [showMessage, setShowMessage] = useState(false);

    useEffect(() => {
        fetchDataFromServer().then(() => console.log("Articles fetched"));
    }, []);

    async function fetchSimilar(author) {
        try {
            const similarResponse = await fetch(`${serveur}/similar?author=${author}`);
            if (!similarResponse.ok) alert("Failed to fetch similar articles");

            const dataSim = await similarResponse.json();
            return dataSim.filter((item) => item.id !== Id);
        } catch (error) {
            console.error("Error fetching similar articles:", error.message);
        }
    }

    async function fetchDataFromServer() {
        try {
            const response = await fetch(`${serveur}/${routeType}?Id=${Id}`);
            if (!response.ok) alert(`Failed to fetch data: ${response.statusText}`);

            const jsonData = await response.json();
            let similarItems;

            if (routeType === "movie") {
                const actorPromises = jsonData.actors.map(async (actor) => {
                    return await fetchSimilar(actor.name);
                });

                const actorResults = await Promise.all(actorPromises);
                similarItems = actorResults.flat();
            } else {
                similarItems = await fetchSimilar(jsonData.author);
            }

            if (similarItems.length === 0) {
                setShowMessage(true);
                setMessage("This author has no other available works.");
            } else {
                setSameAuthor(similarItems);
            }

            setFetchData(jsonData);
        } catch (error) {
            console.error(`Error in getData: ${error.message}`);
        }
    }

    return (
        fetchData &&
        <main className="container">
            {routeType === "album" ? (
                <AlbumDetails album={fetchData}/>
            ) : routeType === "movie" ? (
                <MovieDetails movie={fetchData}/>
            ) : (
                <BookDetails book={fetchData}/>
            )}
            <div className="title is-4 has-text-centered">Works by the Same Author</div>
            <div>
                {showMessage ? (
                    <div className="has-text-centered title is-5">
                        <h3 className="subtitle is-4">{message}</h3>
                    </div>
                ) : (
                    <div className="columns">
                        {sameAuthor.map((e) => (
                            <Similar key={e.title} similar={e}/>
                        ))}
                    </div>
                )}
            </div>
        </main>
    );
}

export {Details};
