import React, {useContext, useState} from "react";
import "../css/login.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCalendarDays} from "@fortawesome/free-solid-svg-icons";
import {Context} from "../App";
import {useNavigate} from "react-router-dom";
import {IS_BOOK, serveur} from "../CommonFunction/constantes.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {useNotification} from "../CommonFunction/CommunFunction.js";

function BookDetails({book}) {
    const [isModalVisible, setModalVisibility] = useState(false);
    const [selectedVolume, setSelectedVolume] = useState("");
    const context = useContext(Context);
    const navigate = useNavigate();

    const handleVolumeChange = (e) => setSelectedVolume(e.target.value);
    const {
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        showSuccess,
    } = useNotification();

    const closeModal = () => setModalVisibility(false);
    const openModal = () => {
        if (context.token) setModalVisibility(true);
        else {
            localStorage.setItem("path", window.location.pathname);
            navigate("/connexion");
        }
    };

    async function reserveArticle() {
        const state = book.bookState.filter((e) => e.volume === selectedVolume)[0].availability;
        if (state === "Reserved") {
            setModalVisibility(false);
            showSuccess("This book is already reserved.");
            return;
        } else if (state === "Borrowed") {
            setModalVisibility(false);
            showSuccess("This book is already borrowed.");
            return;
        }

        try {
            const response = await fetch(`${serveur}/reservation?Id=${book.id}&IdNum=${selectedVolume}&Type=${IS_BOOK}`, {
                method: "POST",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
            });

            const data = await response.json();

            setModalVisibility(false);
            showSuccess(data.message);

        } catch (err) {
            console.error(err);
            showSuccess("An unexpected error occurred.");
        }
    }

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>{book.title}</h1>
            </div>

            <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                          setIsSuccessVisible={setIsSuccessVisible} page={`book/${book.id}`}/>

            <div>
                <br/>
            </div>
            <div className="columns is-mobile is-multiline">
                <div className="column is-3-desktop is-12-mobile">
                    <img src={`${serveur}/cover/${book.coverURL}`} alt={book.title} style={{width: "100%"}}/>
                </div>
                <div className="column is-9-desktop is-8-mobile">
                    <div>
                        <br/>
                    </div>
                    <span className="subtitle is-5">
                        <strong>Title: </strong>{book.title}<br/>
                        <strong>ISBN: </strong>{book.ISBN}<br/>
                        <strong>Author: </strong>{book.author}<br/>
                        <strong>Language: </strong>{book.language}<br/>
                        <strong>Document Type: </strong>{book.type}<br/>
                        {
                            (book.type === "Roman") ?
                                <span>
                                    <strong>Number of Pages: </strong>{book.nbrPages}<br/>
                                </span> :
                                <span>
                                    <strong>Number of Volumes: </strong>{book.nbrPages}<br/>
                                </span>
                        }
                        <strong>Genres: </strong>{book.genres.map((e) => e.value).join(", ")} <br/>
                        <strong>Publication Date: </strong>{book.releaseDate}<br/>
                    </span>
                </div>
                <div className="column is-12">
                    <span className="subtitle is-5"><strong>Plot : </strong>{book.plot}<br/></span>
                </div>

                <div className="column is-12">
                    <div style={{
                        overflow: "hidden",
                        padding: "0px",
                        borderBottom: "1px solid rgb(224, 224, 224)",
                        textAlign: "left"
                    }}>
                        <div style={{display: "inline-block"}}>
                            <div>
                                <span id="title">
                                    <h3><strong>Copies</strong></h3>
                                </span>
                            </div>
                        </div>
                        <div style={{float: "right"}}>
                            <button className="button" onClick={openModal}>
                                <div className="columns is-mobile is-multiline">
                                    <span className="column is-12">
                                        <FontAwesomeIcon icon={faCalendarDays}/>
                                        <br/>
                                        Reserve
                                    </span>
                                </div>
                            </button>
                        </div>

                        <div className={isModalVisible ? "modal is-active" : "modal"}>
                            <div className="modal-background" onClick={closeModal}></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">Reservation Request</p>
                                    <button className="delete" aria-label="close" onClick={closeModal}></button>
                                </header>
                                <section className="modal-card-body">
                                    If this title has more than one volume, please select the volume you wish to
                                    reserve.
                                    <br/>
                                    <br/>

                                    {
                                        book.bookState.map(
                                            (e) =>
                                                <label className="subtitle is-5" key={e.volume}
                                                       style={{height: "10px"}}>
                                                    <span>
                                                        <span>
                                                            <input name="volume" type="radio" value={e.volume}
                                                                   onChange={handleVolumeChange}/>
                                                        </span>
                                                    </span>
                                                    &nbsp;&nbsp;
                                                    <span>{e.volume}</span>
                                                </label>
                                        )
                                    }

                                    <br/>
                                </section>
                                <div className="modal-card-foot">
                                    <button className="button is-link is-light" onClick={reserveArticle}>
                                        CONFIRM
                                    </button>
                                    <button className="button" onClick={closeModal}>
                                        CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>

                        <ul>
                            {
                                book.bookState.map(
                                    (e) =>
                                        <li key={e.volume}>
                                            <span className="subtitle is-5">
                                                {book.title} - {e.volume} <br/>
                                                <u>{e.availability}</u>
                                            </span>
                                            <hr/>
                                        </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </main>
    );
}

export {BookDetails};
