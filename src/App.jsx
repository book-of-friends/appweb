import React, {useState} from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";

import {Login} from "./SignPage/Login.jsx";
import {Signup} from "./SignPage/Signup";
import {MdpOublie} from "./mdp-oublie.jsx";
import {AddBook} from "./Gestion/AddBook.jsx";
import {AddAlbum} from "./Gestion/AddAlbum.jsx";
import {NoAuth} from "./ErrorPages/NoAuth.jsx";
import {NoToken} from "./ErrorPages/NoToken.jsx";
import {NoMatch} from "./ErrorPages/NoMatch.jsx";
import {Header} from "./Header/Header.jsx";
import {Footer} from "./Footer";
import {HomePage} from "./HomePage/HomePage";
import {AddMovie} from "./Gestion/AddMovie.jsx";
import {ProductHome} from "./HomePage/ProductHome";
import {Details} from "./Details/Details";
import {AdminPage} from "./HomePage/AdminPage";
import {Reservation} from "./Reservations/Reservation";
import {ReservationAdmin} from "./Reservations/ReserveAdmin";
import {Commentaire} from "./Commentaires/Commentaires";
import {CommentaireAdmin} from "./Commentaires/CommentaireAdmin";
import {UserDashboard} from "./UserPage/UserDashboard.jsx";

export const Context = React.createContext(undefined);

export default function App() {

    const [admin, setAdmin] = useState(localStorage.getItem("admin") === "true" || false);
    const [token, setToken] = useState(localStorage.getItem("token") || "");

    const ObjContext = {admin, setAdmin, token, setToken};

    return (
        <Context.Provider value={ObjContext}>
            <BrowserRouter>
                <br/>
                <Header/>
                <Routes>
                    <Route path="/connexion" element={<Login/>}/>
                    <Route path="/" element={admin ? <AdminPage/> : <HomePage/>}/>
                    <Route path="/inscription" element={<Signup/>}/>
                    <Route path="/user-dashboard" element={token === "" ? <NoToken/> : <UserDashboard/>}/>
                    <Route path="/my-reservations" element={token === "" ? <NoToken/> : <Reservation/>}/>
                    <Route path="/mot-passe-oublie" element={<MdpOublie/>}/>
                    <Route path="/ajouter-livre" element={(admin) ? <AddBook/> : <NoAuth/>}/>
                    <Route path="/modifier-livre/:idBook" element={(admin === true) ? <AddBook/> : <NoAuth/>}/>
                    <Route path="/ajouter-album" element={(admin === true) ? <AddAlbum/> : <NoAuth/>}/>
                    <Route path="/modifier-album/:idAlbum" element={(admin === true) ? <AddAlbum/> : <NoAuth/>}/>
                    <Route path="/ajouter-film" element={(admin === true) ? <AddMovie/> : <NoAuth/>}/>
                    <Route path="/modifier-film/:idMovie" element={(admin === true) ? <AddMovie/> : <NoAuth/>}/>
                    <Route path="/reservations" element={(admin === true) ? <ReservationAdmin/> : <NoAuth/>}/>
                    <Route path="/commentaires"
                           element={token === "" ? <NoToken/> : (admin === true) ? <CommentaireAdmin/> :
                               <Commentaire/>}/>
                    <Route path="/products" element={<ProductHome/>}/>
                    <Route path="/movies" element={<ProductHome/>}/>
                    <Route path="/albums" element={<ProductHome/>}/>
                    <Route path="/mangas" element={<ProductHome/>}/>
                    <Route path="/romans" element={<ProductHome/>}/>
                    <Route path="/books" element={<ProductHome/>}/>
                    <Route path="/album/:Id" element={<Details/>}/>
                    <Route path="/book/:Id" element={<Details/>}/>
                    <Route path="/movie/:Id" element={<Details/>}/>
                    <Route path="*" element={<NoMatch/>}/>
                </Routes>
                <Footer/>
            </BrowserRouter>
        </Context.Provider>
    );
}
