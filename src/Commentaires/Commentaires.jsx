import React, {useState} from "react";
import "../css/signup.css";
import {serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {ErrorModel} from "../ErrorModel.jsx";

function Commentaire() {
    const [message, setMessage] = useState("");
    const {
        isErrorVisible,
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        error,
        showSuccess,
        showError
    } = useNotification();

    async function sendComment() {
        try {
            const body = {
                comment: message,
            };

            const response = await fetch(`${serveur}/comment`, {
                method: "POST",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(body),
            });
            const data = await response.json();

            if (response.ok) showSuccess(data.message);
            else showError(data.message);

        } catch (err) {
            showError(err.message);
        }
    }

    function handleChangeMessage(event) {
        setMessage(event.target.value);
    }

    return (
        <main id="Contenu">
            <div className="form-control">
                <div className="has-text-centered">
                    <h1>Your comment!</h1>
                </div>

                <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                              setIsSuccessVisible={setIsSuccessVisible} page={""}/>

                <fieldset>
                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="plot">
                                <span>Description</span>
                            </label>
                            <textarea
                                placeholder="Your comment here..."
                                type="text" id="plot"
                                rows="8" name="plot"
                                onChange={handleChangeMessage}
                                required/>
                        </div>
                    </div>

                </fieldset>
                <br/>

                <ErrorModel error={error} isErrorVisible={isErrorVisible}/>

                <br/>
                <div className="has-text-centered">
                    <input
                        className="button submit"
                        id="ajouter"
                        onClick={sendComment}
                        value="Send"
                        style={{cursor: "pointer"}}
                    />
                </div>
                <br/>
            </div>
            <br/>
        </main>
    );
}

export {Commentaire};
