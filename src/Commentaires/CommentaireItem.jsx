function ItemCommentaire({item}) {

    return (
        <div className="card columns is-mobile">
            <div className="column is-8-desktop is-6-mobile mb-1">
                <span className="has-text-weight-bold">Comment: </span> {item.comment} <br/>
            </div>
        </div>
    );
}

export {ItemCommentaire};
