import React, {useEffect, useState} from "react";
import {ItemCommentaire} from "./CommentaireItem";
import {serveur} from "../CommonFunction/constantes.js";
import {User} from ".././CommonFunction/user.js";

function CommentaireAdmin() {
    const [fetchAll, setFetchAll] = useState([]);
    const [message, setMessage] = useState("");
    const [isMessageVisible, setIsMessageVisible] = useState(false);
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    useEffect(() => {
        fetchCommentaires().then(() => console.log("Commentaires fetched"));
    }, []);

    async function fetchCommentaires() {
        try {
            const response = await fetch(`${serveur}/comment`, {
                method: "GET",
                headers: {
                    authorization: `Bearer ${User.TOKEN}`,
                    "Content-Type": "application/json",
                },
            });
            const data = await response.json();

            if (response.ok) {
                setFetchAll(data);
            } else if (response.status === 404) {
                setMessage(data.message);
                setIsMessageVisible(true);
            } else {
                setError(true);
                setErrorMessage(data.message);
            }
        } catch (err) {
            alert("An error occurred. Please try again.");
        }
    }

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>subscribers comments</h1>
            </div>
            {error &&
                <div id="messageBTN" className="modal is-active is-hidden">
                    <div className="modal-background" aria-disabled="true"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button className="delete" aria-label="delete" onClick={() => setError(false)}></button>
                            </div>
                            <div className="message-body">
                                {errorMessage}
                            </div>
                        </article>
                    </div>
                </div>
            }
            <br/>
            <div></div>

            <div>
                {
                    !isMessageVisible ?
                        fetchAll.map((item) => <ItemCommentaire item={item} key={item.comment}/>) :

                        <div className="has-text-centered title is-5">
                            <h3 className="subtitle is-4">{message}</h3>
                        </div>

                }
            </div>

        </main>
    );
}

export {CommentaireAdmin};
