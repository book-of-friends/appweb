import React, {useContext, useEffect, useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faExclamationCircle, faLock} from "@fortawesome/free-solid-svg-icons";
import {Context} from "../App.jsx";
import "../css/login.css";
import {serveur} from "../CommonFunction/constantes.js";

function Login() {
    const {token} = useContext(Context);
    const navigate = useNavigate();

    useEffect(() => {
        if (token !== "") navigate("/");
    }, [token, navigate]);

    const lecontext = useContext(Context);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState({visible: false, message: ""});

    const handleInputChange = (event, setState) => {
        setState(event.target.value);
    };

    const handleLogin = async () => {
        try {
            const body = {
                email,
                password,
            };
            const response = await fetch(`${serveur}/auth/token`, {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(body),
            });

            const data = await response.json();

            if (response.ok) {
                localStorage.setItem("token", data.token);
                localStorage.setItem("admin", data.admin);

                lecontext.setToken(data.token);
                lecontext.setAdmin(data.admin);
                navigate("/");
            } else {
                setError({visible: true, message: data.message});
            }
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <main id="Contenu">
            <div>
                <h1 className="has-text-centered">Log in</h1>
                <div id="loginForm">
                    <div>
                        <label htmlFor="email">Your email</label>
                        <div className="control has-icons-left">
                            <input
                                type="email"
                                id="email"
                                className="input w100"
                                name="email"
                                onChange={(event) => handleInputChange(event, setEmail)}
                                autoComplete="email"
                                required
                            />
                            <span className="icon is-left">
                                <FontAwesomeIcon icon={faEnvelope} size="lg"/>
                            </span>
                        </div>
                    </div>
                    <div>
                        <label htmlFor="pwd">Your password</label>
                        <div className="control has-icons-left">
                            <input
                                type="password"
                                id="pwd"
                                className="input w100"
                                name="password"
                                onChange={(event) => handleInputChange(event, setPassword)}
                                autoComplete="current-password"
                                required
                            />
                            <span className="icon is-left">
                                <FontAwesomeIcon icon={faLock} size="lg"/>
                            </span>
                        </div>
                        <p>
                            <Link id="right" to="/mot-passe-oublie">
                                Forgot password?
                            </Link>
                        </p>
                    </div>

                    <div className={`form-element erreur ${error.visible ? "" : "is-hidden"}`} id="erreur">
                        <h2>
                            &nbsp;&nbsp;
                            <FontAwesomeIcon icon={faExclamationCircle}/> &nbsp; Erreur dans le formulaire
                        </h2>
                        <ul>
                            <li id="message">{error.message}</li>
                        </ul>
                    </div>

                    <div className="buttonGroup">
                        <button
                            className="form-element"
                            id="submit"
                            onClick={handleLogin}
                            data-target="modal-ter"
                            aria-haspopup="true"
                        >
                            Log in
                            <span aria-hidden="true"> &gt; </span>
                        </button>
                        <span> </span>
                        <Link to="/inscription" className="buttonSecondaire">
                            Sign up <span aria-hidden="true"> &gt; </span>
                        </Link>
                    </div>
                </div>
            </div>
        </main>
    );
}

export {Login};
