import React, {useContext, useEffect, useState} from "react";
import "../css/signup.css";
import {Context} from "../App";
import {useNavigate} from "react-router-dom";
import {serveur} from "../CommonFunction/constantes.js";
import {VerificationConfirmation, VerificationPass} from "../Verification.jsx";

function Signup() {
    const lecontext = useContext(Context);
    const navigate = useNavigate();

    useEffect(() => {
        if (lecontext.token !== "") {
            navigate("/");
        }
    }, [lecontext.token, navigate]);

    const [formData, setFormData] = useState({
        firstname: "", lastname: "", address: "", city: "",
        province: "Quebec", country: "Canada", codePostal: "", phone: "",
        email: "", password: "", confPassword: "",
        dateOfBirth: "",
    });

    const [message, setMessage] = useState("");
    const [isModalVisible, setModalVisibility] = useState(false);

    const handleChange = (event) => {
        const {name, value} = event.target;
        setFormData((prevData) => ({...prevData, [name]: value}));
    };

    const handleSubmit = async () => {
        try {
            const response = await fetch(`${serveur}/auth/register`, {
                headers: {"Content-Type": "application/json"},
                method: "POST",
                body: JSON.stringify(formData),
            });

            const data = await response.json();

            setMessage(data.message);
            setModalVisibility(true);
        } catch (err) {
            console.error("Une erreur s'est produite");
        }
    };

    return (
        <main id="Contenu">
            <div className="form-control">
                <div>
                    <h1 className="has-text-centered">Subscribe!</h1>
                </div>

                <div id="messageBTN" className={`modal ${isModalVisible ? "is-active" : ""}`}>
                    <div className="modal-background" aria-disabled="true"></div>
                    <div className="modal-card">
                        <article className="message is-info">
                            <div className="message-header">
                                <p>Info</p>
                                <button
                                    className="delete"
                                    aria-label="delete"
                                    onClick={() => {
                                        setModalVisibility(false);
                                        // navigate("/connexion");
                                    }}
                                ></button>
                            </div>
                            <div className="message-body">{message}</div>
                        </article>
                    </div>
                </div>

                <fieldset>
                    <legend>
                        <span className="fieldset-legend">FULL-NAME AND DATE OF BIRTH</span>
                    </legend>
                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="firstname">
                                <span>First Name</span>
                            </label>
                            <input
                                type="text"
                                id="firstname"
                                name="firstname"
                                className="form-control"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="lastname">
                                <span>Last Name</span>
                            </label>
                            <input
                                type="text"
                                id="lastname"
                                name="lastname"
                                className="form-control"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="dateOfBirth">
                                <span>Date Of Birth</span>
                            </label>
                            <input
                                min="1903-01-26"
                                data-min-year="1903"
                                max="2023-01-26"
                                data-max-year="2023"
                                type="date"
                                data-drupal-date-format="Y-m-d"
                                data-drupal-selector="ddn"
                                id="dateOfBirth"
                                name="dateOfBirth"
                                className="form-control"
                                required
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                </fieldset>
                <br/>

                <fieldset>
                    <legend>
                        <span className="fieldset-legend">ADDRESS</span>
                    </legend>
                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="address">
                                <span>Residential Address</span>
                            </label>
                            <input
                                type="text"
                                id="address"
                                name="address"
                                className="form-control"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="city">
                                <span>City</span>
                            </label>
                            <input
                                type="text"
                                id="city"
                                name="city"
                                className="form-control"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="codePostal">
                                <span>Code postal</span>
                            </label>
                            <input
                                className="form-control"
                                type="text"
                                id="codePostal"
                                name="codePostal"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="country">
                                <span>Country</span>
                            </label>
                            <input
                                className="form-control"
                                value="Canada"
                                type="text"
                                id="country"
                                name="country"
                                disabled
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="province">
                                <span>Province</span>
                            </label>
                            <input
                                className="form-control"
                                value="Quebec"
                                type="text"
                                id="province"
                                name="province"
                                disabled
                            />
                        </div>
                    </div>
                </fieldset>
                <br/>

                <fieldset>
                    <legend>
                        <span className="fieldset-legend">COMMUNICATION</span>
                    </legend>

                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="phone">
                                <span>Phone</span>
                            </label>
                            <input
                                mask="999-999-9999"
                                className="form-control"
                                type="text"
                                id="phone"
                                name="phone"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="email">
                                <span>Email</span>
                            </label>
                            <input
                                className="form-control"
                                type="email"
                                id="email"
                                name="email"
                                required
                                onChange={handleChange}
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="password">
                                <span>Password</span>
                            </label>
                            <input
                                className="form-control"
                                type="password"
                                id="password"
                                name="password"
                                required
                                onChange={handleChange}
                            />
                            <VerificationPass password={formData.password}/>
                        </div>
                        <div id="space">
                            <label htmlFor="confPassword">
                                <span>Password Confirmation</span>
                            </label>
                            <input
                                className="form-control"
                                type="password"
                                id="confPassword"
                                name="confPassword"
                                required
                                onChange={handleChange}
                            />
                            <VerificationConfirmation password={formData.password}
                                                      confPassword={formData.confPassword}/>
                        </div>
                    </div>
                </fieldset>
                <br/>

                <div className="columns is-mobile is-centered">
                    <input
                        className="button submit"
                        onClick={handleSubmit}
                        placeholder="Sign Up"
                    />
                </div>
                <br/>
            </div>
        </main>
    );
}

export {Signup};
