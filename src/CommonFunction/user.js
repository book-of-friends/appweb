import {serveur} from "./constantes.js";

export class User {

    static TOKEN = localStorage.getItem("token");

    async getPersonnalInfo() {
        try {
            const response = await fetch(`${serveur}/user`, {
                method: "GET",
                headers: {authorization: `Bearer ${User.TOKEN}`},
            });
            const data = await response.json();
            if (response.ok) {
                return {
                    success: true,
                    message: null,
                    data,
                };
            } else {
                return {
                    success: false,
                    message: data.message,
                    data: null,
                };
            }
        } catch (err) {
            alert(err);
        }
    }

}
