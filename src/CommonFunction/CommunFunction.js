import {useState} from "react";

export function useNotification() {
    const [isErrorVisible, setIsErrorVisible] = useState(false);
    const [isSuccessVisible, setIsSuccessVisible] = useState(false);
    const [success, setSuccess] = useState("");
    const [error, setError] = useState("");

    function showSuccess(message) {
        setIsErrorVisible(false);
        setIsSuccessVisible(true);
        setSuccess(message);
    }

    function showError(errorMessage) {
        setIsErrorVisible(true);
        setError(errorMessage);
    }

    return {
        isErrorVisible,
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        error,
        showSuccess,
        showError,
    };
}
