async function makeAlbumRequest(url, method, body, showSuccess, showError) {
    try {
        const response = await fetch(url, {
            method,
            headers: {
                authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body),
        });

        const data = await response.json();

        if (response.ok) {
            showSuccess(data.message);
        } else {
            showError(data.message + "\n" + data.additionalInfo);
        }
    } catch (error) {
        console.error(error);
        showError("An unexpected error occurred.");
    }
}

export {makeAlbumRequest};