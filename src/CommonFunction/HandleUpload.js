import {serveur} from "./constantes.js";

const handleUpload = async (file, showError) => {
    try {
        const formData = new FormData();
        formData.append("file", file);

        console.log(formData.get("file"));
        const response = await fetch(`${serveur}/cover`, {
            method: "POST",
            body: formData,
        });

        if (response.ok) {
            const data = await response.json();
            return data.filename;
        } else {
            showError(response.message); // Use the showError function from useNotification
        }
    } catch (error) {
        console.error(error);
        showError("An unexpected error occurred.");
    }
};

export {handleUpload};