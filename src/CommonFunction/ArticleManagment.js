import {serveur} from "./constantes.js";

async function deleteArticle(showSuccess, itemType, itemId) {
    try {
        const response = await fetch(`${serveur}/${itemType.toLowerCase()}?Id=${itemId}`, {
            method: "DELETE",
            headers: {
                authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
        });
        const data = await response.json();

        if (response.ok) showSuccess(data.message);
        else showSuccess(data.message);

    } catch (err) {
        console.error(err);
        showSuccess("An unexpected error occurred.");
    }
}

export {deleteArticle};
