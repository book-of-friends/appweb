const serveur = "http://localhost:3000";

const genreList = [
    {value: "Fantasy", label: "Fantasy"},
    {value: "Adventure", label: "Adventure"},
    {value: "Romance", label: "Romance"},
    {value: "Horror", label: "Horror"},
    {value: "Thriller", label: "Thriller"},
    {value: "Mystery", label: "Mystery"},
    {value: "Drama", label: "Drama"},
    {value: "Action", label: "Action"},
    {value: "Comedy", label: "Comedy"},
    {value: "Suspense", label: "Suspense"},
    {value: "Paranormal", label: "Paranormal"},
    {value: "Science Fiction", label: "Science Fiction"},
    {value: "Children", label: "Children"},
    {value: "Memoir", label: "Memoir"},
    {value: "Health", label: "Health"},
    {value: "Historical", label: "Historical"},
    {value: "Epic Fantasy", label: "Epic Fantasy"},
    {value: "Romantic Comedy", label: "Romantic Comedy"},
    {value: "Biography", label: "Biography"},
    {value: "Documentary", label: "Documentary"},
    {value: "Musical", label: "Musical"},
];

const IS_BOOK = "book";
const IS_MOVIE = "movie";
const IS_ALBUM = "album";

export {serveur, genreList, IS_ALBUM, IS_BOOK, IS_MOVIE};
