import React, {useState} from "react";
import {BsArrowLeftCircleFill, BsArrowRightCircleFill} from "react-icons/bs";
import "../css/Carousel.css";
import {Link} from "react-router-dom";

export const Carousel = ({data}) => {
    const [currentSlide, setCurrentSlide] = useState(0);

    const changeSlide = (direction) => {
        const newSlide = direction === "next" ? (currentSlide === data.length - 1 ? 0 : currentSlide + 1) : (currentSlide === 0 ? data.length - 1 : currentSlide - 1);
        setCurrentSlide(newSlide);
    };

    return (
        <div className="carousel">
            <BsArrowLeftCircleFill className="arrow arrow-left" onClick={() => changeSlide("prev")}/>
            {
                data.map(({link, src, alt}, idx) => {
                    return (
                        <Link to={link} key={idx} className={`slide ${currentSlide === idx ? "" : "hidden"}`}>
                            <img src={src} alt={alt}/>
                        </Link>
                    );

                })

            }
            <BsArrowRightCircleFill className="arrow arrow-right" onClick={() => changeSlide("next")}/>
            <span className="indicators">
                {
                    data.map((_, idx) => {
                        return (
                            <button key={idx} onClick={() => setCurrentSlide(idx)}
                                    className={`indicator ${currentSlide === idx ? "" : "inactive"}`}></button>
                        );
                    })
                }
            </span>
        </div>
    );
};