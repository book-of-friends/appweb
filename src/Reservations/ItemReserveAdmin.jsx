import {IS_ALBUM, serveur} from "../CommonFunction/constantes.js";
import React from "react";

function ItemReserveAdmin({item}) {
    return (
        <div className={`card columns is-mobile ${item.type === "Album" ? IS_ALBUM : "default"}`}>
            <figure className={`content column is-4-mobile is-2-desktop`}>
                &nbsp;&nbsp;<img src={`${serveur}/cover/${item.coverURL}`} alt={item.article} width="150" height="100"/>
            </figure>

            <div className="column is-8-desktop is-6-mobile mb-1">
                <br />
                {item.type === "albums" ?
                    <p className="title is-4">{item.title}</p>
                    :
                    <p className="title is-4">{item.title} - {item.articleNumber}</p>
                }
                <div></div>
                <span className="subtitle is-6">
                    <span className="has-text-weight-bold">Author: </span>{item.author} <br/>
                    <span className="has-text-weight-bold">Document Type: </span>{item.type} <br/>
                    <p className="subtitle is-6">
                        <span className="has-text-weight-bold">{item.state} by: </span>{item.user.name}<br/>
                        <span className="has-text-weight-bold">Borrowed on: </span>{item.dateBorrow ? item.dateBorrow : "NDA"} <br/>
                        <span className="has-text-weight-bold">Return Date: </span>{item.dateReturn ? item.dateReturn : "NDA"} <br/>
                    </p>
                </span>
            </div>
        </div>
    );
}

export { ItemReserveAdmin };
