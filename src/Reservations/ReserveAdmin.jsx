import React, { useEffect, useState } from "react";
import "../css/HomePage.css";
import { ItemReserveAdmin } from "./ItemReserveAdmin";
import { serveur } from "../CommonFunction/constantes.js";
import { User } from "../CommonFunction/user.js";

function ReservationAdmin() {
    const [fetchAll, setFetchAll] = useState([]);
    const [message, setMessage] = useState("");
    const [showMessage, setShowMessage] = useState(false);

    // Fetch reservations from the server
    async function getReservations() {
        try {
            const response = await fetch(`${serveur}/reservations`, {
                method: "GET",
                headers: {
                    authorization: `Bearer ${User.TOKEN}`,
                    "Content-Type": "application/json",
                },
            });

            const data = await response.json();

            if (response.ok) {
                setFetchAll(data);
            } else {
                setShowMessage(true);
                setMessage(data.message);
            }
        } catch (error) {
            console.error("Error fetching reservations:", error.message);
            setShowMessage(true);
            setMessage("An error occurred while fetching reservations.");
        }
    }

    // Fetch reservations on component mount
    useEffect(() => {
        getReservations().then(() => console.log("Reservations fetched"));
    }, []);

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>Documents reserved by subscribers</h1>
            </div>
            <br />
            {showMessage ? (
                <div className="has-text-centered title is-5">
                    <h3 className="subtitle is-4">{message}</h3>
                </div>
            ) : (
                fetchAll.map((item) => (
                    <ItemReserveAdmin item={item} key={item.article} />
                ))
            )}
        </main>
    );
}

export { ReservationAdmin };
