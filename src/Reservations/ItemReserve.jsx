import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React, {useState} from "react";
import {IS_ALBUM, serveur} from "../CommonFunction/constantes.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {useNotification} from "../CommonFunction/CommunFunction.js";

function ItemReserve({item}) {
    const { isSuccessVisible, setIsSuccessVisible, success, showSuccess } = useNotification();
    const [estaffiche, setEstAffiche] = useState(false);

    async function cancelReservation() {
        if (item.state !== "Borrowed") {
            await deleteReservation();
        } else {
            setEstAffiche(false)
            showSuccess("This reservation cannot be canceled because the document is already borrowed.");
        }
    }

    async function deleteReservation() {
        try {
            const response = await fetch(`${serveur}/reservation?Id=${item.idReservation}`, {
                method: "DELETE",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
            });

            const data = await response.json();

            setEstAffiche(false);
            showSuccess(data.message);

        } catch (error) {
            console.error(error);
            showSuccess("An unexpected error occurred.");
        }
    }

    return (
        <div className={`card columns is-mobile ${item.type === "Album" ? IS_ALBUM : "default"}`}>

            <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                          setIsSuccessVisible={setIsSuccessVisible} page={"my-reservations"}/>

            <figure className={`content column is-4-mobile is-2-desktop`}>
                &nbsp;&nbsp;<img src={`${serveur}/cover/${item.coverURL}`} alt={item.article} width="150" height="100"/>
            </figure>

            <div className="column is-8-desktop is-6-mobile mb-1">
                <br />
                <p className="title is-4">{item.article}</p>
                <div></div>
                <span className="subtitle is-6">
                    <span className="has-text-weight-bold">Author: </span>{item.author} <br/>
                    <span className="has-text-weight-bold">Document Type: </span>{item.type} <br/>
                    <p className="subtitle is-6">
                        <span className="has-text-weight-bold">Borrowed on: </span>{item.dateBorrow ? item.dateBorrow : "NDA"} <br/>
                        <span className="has-text-weight-bold">Return Date: </span>{item.dateReturn ? item.dateReturn : "NDA"} <br/>
                        <span className="has-text-weight-bold">{item.type} status: </span> {item.state}
                    </p>
                </span>
            </div>

            <div className="columns is-mobile column is-7 is-vcentered">
                <button className="button is-danger " title="Cancel Reservation" onClick={() => {
                    setEstAffiche(true);
                }}>
                    <FontAwesomeIcon icon={faTimes}/>
                </button>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div id="modal-ter" className={`modal ${estaffiche ? "is-active" : ""}`}>
                    <div className="modal-background"></div>
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">ATTENTION!!</p>
                            <button className="delete" aria-label="close" onClick={() => { setEstAffiche(false) }}></button>
                        </header>
                        <section className="modal-card-body">Would you cancel this reservation??</section>
                        <div className="modal-card-foot">
                            <button className="button is-danger" onClick={ async () => { await cancelReservation() }}>YES</button>
                            <button className="button" onClick={() => { setEstAffiche(false) }}>NO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export {ItemReserve};
