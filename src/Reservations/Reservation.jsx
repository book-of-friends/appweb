import React, { useEffect, useState } from "react";
import { ItemReserve } from "./ItemReserve";
import "../css/HomePage.css";
import { serveur } from "../CommonFunction/constantes.js";

function Reservation() {
    const [reservations, setReservations] = useState([]);
    const [message, setMessage] = useState("");
    const [showMessage, setShowMessage] = useState(false);

    async function getReservation() {
        try {

            const response = await fetch(`${serveur}/reservation`, {
                method: "GET",
                headers: {
                    authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json"
                },
            });
            const data = await response.json();
            if (response.ok) {
                setReservations(data);
            } else {
                setShowMessage(true);
                setMessage(data.message);
            }
        } catch (error) {
            console.error("Error fetching reservations:", error.message);
            setShowMessage(true);
            setMessage("An error occurred while fetching reservations.");
        }
    }

    useEffect(() => {
        getReservation().then(() => console.log("Reservations fetched"));
    }, []);

    return (

        <main className="container">
            <div className="has-text-centered">
                <h1>My reservations</h1>
            </div>
            <br />
            <div>
                {showMessage ? (
                    <div className="has-text-centered title is-5">
                        <h3 className="subtitle is-4">{message}</h3>
                    </div>
                ) : (
                    reservations.map((item) => (
                        <ItemReserve item={item} key={item.article}/>
                    ))
                )}
            </div>
        </main>
    );
}

export {Reservation};
