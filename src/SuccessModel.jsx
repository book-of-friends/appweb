import React from "react";
import "./css/signup.css";
import {useNavigate} from "react-router-dom";

function SuccessModel({success, isSuccessVisible, setIsSuccessVisible, page}) {

    const navigate = useNavigate();

    function handleNavigation() {
        setIsSuccessVisible(false);
        navigate(`/${page}`);
    }

    return (
        <div className={`modal is-active ${isSuccessVisible ? "" : "is-hidden"}`}>
            <div className="modal-background" aria-disabled="true"></div>
            <div className="modal-card">
                <article className="message is-info">
                    <div className="message-header">
                        <p>Info</p>
                        <button
                            className="delete"
                            aria-label="delete"
                            onClick={handleNavigation}
                        ></button>
                    </div>
                    <div className="message-body">{success}</div>
                </article>
            </div>
        </div>
    );
}

export {SuccessModel};
