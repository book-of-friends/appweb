import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEnvelope, faExclamationCircle} from "@fortawesome/free-solid-svg-icons";
import {serveur} from "./CommonFunction/constantes.js";
import "./css/login.css";
import {VerificationConfirmation, VerificationPass} from "./Verification.jsx";

function MdpOublie() {
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmation, setConfirmation] = useState("");
    const [isPwdModalVisible, setPwdModalVisible] = useState(false);
    const [isSuccessModalVisible, setSuccessModalVisible] = useState(false);
    const [message, setMessage] = useState("");
    const [nip, setNip] = useState("");
    const [code, setCode] = useState(0);
    const [isNipModalVisible, setNipModalVisible] = useState(false);
    const [error, setError] = useState({visible: false, message: ""});

    const handleChangeCourriel = (event) => setEmail(event.target.value);
    const handleNipChange = (event) => setNip(event.target.value);
    const handlePasswordChange = (event) => setPassword(event.target.value);
    const handleConfirmationChange = (event) => setConfirmation(event.target.value);

    const showMessage = (message) => {
        setMessage(message);
        setNipModalVisible(true);
    };

    const generateNip = () => Math.floor(Math.random() * 1000000);

    const handleNipConfirmation = async () => {
        setError({visible: false, message: ""});

        try {
            const sended = generateNip();
            setCode(sended);

            const body = {
                email: email,
                code: sended,
            };

            const response = await fetch(`${serveur}/user/password-oublie`, {
                headers: {"Content-Type": "application/json"},
                method: "POST",
                body: JSON.stringify(body),
            });

            const data = await response.json();

            if (response.ok) {
                showMessage(data.message);
            } else {
                setError({visible: true, message: data.message});
            }
        } catch (error) {
            console.error("Error:", error);
        }
    };

    const isWeakPassword = () => {
        const isShort = password.length < 8;
        const hasUppercase = /[A-Z]/.test(password);
        const hasLowercase = /[a-z]/.test(password);
        const hasDigit = /\d/.test(password);
        const hasSpecialChar = /[!@#$%^&*?]/.test(password);
        const isConfirmation = password === confirmation;

        return isShort || !hasUppercase || !hasLowercase || !hasDigit || !hasSpecialChar || !isConfirmation;
    };

    const handlePasswordReset = async () => {
        try {
            const body = {
                email: email,
                newPassword: password,
            };

            const response = await fetch(`${serveur}/user/reset-password`, {
                headers: {"Content-Type": "application/json"},
                method: "POST",
                body: JSON.stringify(body),
            });

            const data = await response.json();

            if (response.ok) {
                setMessage(data.message);
                setSuccessModalVisible(true);
            } else {
                setPwdModalVisible(false);
                setError({visible: true, message: data.message});
            }
        } catch (error) {
            console.error("Error:", error);
        }
    };

    const renderNipModal = () => (
        <div className="modal is-active">
            <div className="modal-background" aria-disabled="true"></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">NIP confirmation</p>
                </header>
                <section className="modal-card-body">
                                <span className="text">
                                    {message}
                                </span>

                    <br/>
                    <div className="field">
                        <label className="label">Enter security code: </label>
                        <div>
                            <input
                                className="input"
                                maxLength="6"
                                value={nip}
                                onChange={handleNipChange}
                                required
                            />
                        </div>
                    </div>

                    <br/>
                    Didn't receive the email? &nbsp;
                    <span className="is-underlined text is-link is-clickable" onClick={handleNipConfirmation}>
                                    Resend a request.
                                </span>
                </section>
                <footer className="modal-card-foot" style={{justifyContent: "flex-end"}}>
                    <button className="button is-danger">Cancel</button>
                    <button
                        className="button is-success"
                        onClick={() => {
                            if (nip === code.toString()) {
                                setNipModalVisible(false);
                                setPwdModalVisible(true);
                            }
                        }}
                        disabled={nip !== code.toString()}>
                        Continue
                    </button>
                </footer>
            </div>
        </div>
    );

    const renderPwdModal = () => (
        <div className="modal is-active">
            <div className="modal-background" aria-disabled="true"></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Password reset</p>
                </header>
                <section className="modal-card-body">
                    <div className="field">
                        <label className="label">Enter your new password: </label>
                        <div>
                            <input
                                placeholder="New password"
                                className="input"
                                type="password"
                                value={password}
                                onChange={handlePasswordChange}
                                required
                            />
                            <VerificationPass password={password}/>
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Enter the password confirmation: </label>
                        <div>
                            <input
                                placeholder="Confirmation"
                                className="input"
                                type="password"
                                value={confirmation}
                                onChange={handleConfirmationChange}
                                required
                            />
                            <VerificationConfirmation password={password} confPassword={confirmation}/>
                        </div>
                    </div>
                </section>
                <footer className="modal-card-foot" style={{justifyContent: "flex-end"}}>
                    <button className="button is-danger" onClick={() => setPwdModalVisible(false)}>Cancel</button>
                    <button className="button is-success" disabled={isWeakPassword()}
                            onClick={handlePasswordReset}>Continue
                    </button>
                </footer>
            </div>
        </div>
    );

    const renderSuccessModal = () => (
        <div id="messageBTN" className={"modal is-active"}>
            <div className="modal-background" aria-disabled="true"></div>
            <div className="modal-card">
                <article className="message is-info">
                    <div className="message-header">
                        <p>Info</p>
                    </div>
                    <div className="message-body">
                        {message}

                        <span
                            className="is-underlined is-link is-pulled-right is-clickable"
                            onClick={() => {
                                setSuccessModalVisible(false);
                                navigate("/connexion");
                            }}
                        > OK </span>
                    </div>
                </article>
            </div>
        </div>
    );


    return (
        <main id="Content">
            <div>
                <h1>Password reset</h1>
                {isNipModalVisible && renderNipModal()}
                {isPwdModalVisible && renderPwdModal()}
                {isSuccessModalVisible && renderSuccessModal()}

                <div id="loginForm">
                    <div>
                        <p>
                            To reset your password, enter the email address associated with your account.<br/>
                            After clicking Submit, you will receive an email with the steps to follow.<br/>
                            This email will contain a nip to verify your identity to proceed to the password's
                            reset.<br/>
                            If you don't receive any email, you can make a new request.<br/>
                        </p> <br/>

                        <div>
                            <label htmlFor="email"> Your email </label>
                            <div className="control has-icons-left">
                                <input
                                    type="email" id="email"
                                    className="input w100"
                                    name="email" onChange={handleChangeCourriel}
                                    autoComplete="email" required
                                />
                                <span className="icon is-left">
                                <FontAwesomeIcon icon={faEnvelope} size="lg"/>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div className={`form-element erreur ${error.visible ? "" : "is-hidden"}`} id="erreur">
                        <h2>
                            &nbsp;&nbsp;
                            <FontAwesomeIcon icon={faExclamationCircle}/> &nbsp; Erreur dans le formulaire
                        </h2>
                        <ul>
                            <li id="message">{error.message}</li>
                        </ul>
                    </div>

                    <div className="buttonGroup">
                        <button className="form-element form-button" id="submit" onClick={handleNipConfirmation}>
                            submit
                            <span aria-hidden="true"> &gt; </span>
                        </button>
                    </div>
                </div>
            </div>
        </main>
    );
}

export {MdpOublie};
