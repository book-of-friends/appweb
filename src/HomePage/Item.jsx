import React from "react";
import {Link} from "react-router-dom";
import "../css/HomePage.css";
import {serveur} from "../CommonFunction/constantes.js";

function Item({item}) {

    const productTypeTitles = {
        "Album": `/album/${item.id}`,
        "Roman": `/book/${item.id}`,
        "Manga": `/book/${item.id}`,
        default: `/movie/${item.id}`,
    };
    const productTypeTitle = productTypeTitles[item.type] || productTypeTitles.default;

    return (
        <Link to={productTypeTitle}>
            <div className={`card columns is-mobile is-clickable ${item.type === "Album" ? "album" : "default"}`}>
                <figure className={`content column is-4-mobile is-2-desktop ${item.type === "Album" ? "album" : ""}`}>
                    <img src={`${serveur}/cover/${item.coverURL}`} alt={item.title} width="150" height="100"/>
                </figure>
                <div className="column is-7 mb-1">
                    <br/>
                    <p className="title is-4">{item.title}</p>
                    <div></div>
                    <span className="subtitle is-6">
                        <span className="has-text-weight-bold">Auteur: </span>{item.author} <br/>
                        <span className="has-text-weight-bold">Type de document: </span>{item.type} <br/>
                        <span className="has-text-weight-bold">Langue: </span>{item.language} <br/>
                        {item.type !== "Album" && (
                            <p className="subtitle is-6">
                                <span className="has-text-weight-bold">Genres: </span>
                                {item.genres.map((e) => e).join(", ")}
                            </p>
                        )}
                    </span>
                </div>
            </div>
        </Link>
    );
}

export {Item};
