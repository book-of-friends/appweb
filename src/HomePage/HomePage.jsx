import React, {useEffect, useState} from "react";
import {Carousel} from "../Carousel/Carousel.jsx";
import {Item} from "./Item";
import {slides} from "../Carousel/CarouselData";
import {serveur} from "../CommonFunction/constantes.js";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../css/HomePage.css";

function HomePage() {
    const [articles, setArticles] = useState([]);
    const [filteredArticles, setFilteredArticles] = useState([]);
    const [isFiltered, setIsFiltered] = useState(false);
    const [searchInput, setSearchInput] = useState("");

    const fetchAllArticles = async () => {
        try {
            const products = await fetch(`${serveur}/products`);

            if (products.ok) {
                const data = await products.json();
                setArticles(data);
                setFilteredArticles(data);
            }
        } catch (error) {
            console.error("Error fetching articles:", error);
        }
    };

    const applyFilters = () => {
        if (searchInput.trim() !== "") {
            const filters = articles.filter((t) => {
                const lowercaseSearch = searchInput.toLowerCase();
                return (
                    t.title.toLowerCase().includes(lowercaseSearch) ||
                    t.author.toLowerCase().includes(lowercaseSearch) ||
                    t.type.toLowerCase().includes(lowercaseSearch) ||
                    t.genres.some((genre) => genre.toLowerCase().includes(lowercaseSearch)) ||
                    t.language.toLowerCase().includes(lowercaseSearch)
                );
            });

            setFilteredArticles(filters);
            setIsFiltered(true);
        } else {
            setFilteredArticles(articles);
            setIsFiltered(false);
        }
    };

    useEffect(() => {
        fetchAllArticles().then(() => console.log("Articles fetched"));
    }, []);

    useEffect(() => {
        applyFilters();
    }, [searchInput]);


    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>Bookstore Book of Friends</h1>
                <br/>
                <div className="field is-horizontal is-centered">
                    <div className="field-body">
                        <div className="field">
                            <p className="control is-expanded">
                                <input
                                    id="titre"
                                    className="input"
                                    style={{width: "50%"}}
                                    type="text"
                                    size="sm"
                                    placeholder="Search for titles, authors, genres, etc..."
                                    value={searchInput}
                                    onChange={(event) => setSearchInput(event.target.value)}
                                />
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <br/>

            <div className={isFiltered ? "is-hidden" : "centered-container"}>
                <Carousel data={slides}/>
                <br/>
            </div>

            <div className={!isFiltered ? "is-hidden" : ""}>
                {
                    filteredArticles.length === 0 ? (
                        <div className="has-text-centered">
                            <br/>
                            <p className="title"> No articles found based on your search criteria. </p>
                        </div>
                    ) : (
                        filteredArticles.map((item) => (
                            <Item item={item} key={item.id}/>
                        ))
                    )
                }
            </div>
        </main>
    );
}

export {HomePage};
