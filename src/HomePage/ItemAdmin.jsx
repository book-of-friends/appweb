import React, {useState} from "react";
import {Link} from "react-router-dom";
import "../css/HomePage.css";
import {IS_ALBUM, IS_BOOK, IS_MOVIE, serveur} from "../CommonFunction/constantes.js";
import {deleteArticle} from "../CommonFunction/ArticleManagment.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {useNotification} from "../CommonFunction/CommunFunction.js";

function ItemAdmin({item}) {

    const [estAffiche, setEstAffiche] = useState(false);
    const {isSuccessVisible, setIsSuccessVisible, success, showSuccess, showError} = useNotification();

    const itemType = () => {
        if (item.type === "Album") return "albums";
        else if (item.type === "Manga" || item.type === "Roman") return "books";
        else return "movies";
    };

    const deleteItem = async () => {
        if (item.type === "Album") await deleteArticle(showError, showSuccess, IS_ALBUM, item.id);
        else if (item.type === "Manga" || item.type === "Roman") await deleteArticle(showError, showSuccess, IS_BOOK, item.id);
        else await deleteArticle(showError, showSuccess, IS_MOVIE, item.id);
        setEstAffiche(false);
        window.scrollTo(0, 0);
    };

    return (
        <div>
            <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                          setIsSuccessVisible={setIsSuccessVisible} page={itemType}/>
            <br/>
            <div className="card columns is-mobile" id={(item.type === "Album" ? IS_ALBUM : "default")}>
                <figure
                    className={`content column is-4-mobile is-2-desktop ${item.type === "Album" ? "album-style" : ""}`}>
                    &nbsp;&nbsp;<img src={`${serveur}/cover/${item.coverURL}`} alt={item.title} width="150"
                                     height="100"/>
                </figure>
                <div className="column is-6-mobile is-7-desktop mb-1">
                    <br/>
                    <p className="title is-4">{item.title}</p>
                    <div></div>
                    <span className="subtitle is-6">
                        <span className="has-text-weight-bold">Author:</span> {item.author} <br/>
                        <span className="has-text-weight-bold">Document Type:</span> {item.type} <br/>
                        <span className="has-text-weight-bold">Language:</span> {item.language} <br/>
                        {item.type !== "Album" ? (
                            <p className="subtitle is-6">
                                <span className="has-text-weight-bold">Genres:</span>{" "}
                                {item.genres.map((genre) => genre).join(", ")}
                            </p>
                        ) : null}
                    </span>
                </div>

                <div className="column is-2-desktop is-2-mobile is-flex is-vcentered">
                    <div className="columns is-mobile is-flex is-vcentered">
                        <Link
                            to={item.type === "Album" ? `/modifier-album/${item.id}` : item.type === "Manga" || item.type === "Roman" ? `/modifier-livre/${item.id}` : `/modifier-film/${item.id}`}>
                            <img src={"./edit.png"} alt={`edit ${item.type}`}/>
                        </Link>
                        <button className="btn" data-target="modal-ter" aria-haspopup="true"
                                onClick={() => setEstAffiche(true)}>
                            <img src={"./delete.png"} alt="delete"/>
                        </button>
                        <div id="modal-ter" className={`modal ${estAffiche ? "is-active" : ""}`}>
                            <div className="modal-background"></div>
                            <div className="modal-card">
                                <header className="modal-card-head">
                                    <p className="modal-card-title">ATTENTION!!</p>
                                    <button className="delete" aria-label="close" onClick={() => {
                                        setEstAffiche(false);
                                    }}></button>
                                </header>
                                <section className="modal-card-body" id="message"> Do you want to delete this work from
                                    the list?
                                </section>
                                <div className="modal-card-foot">
                                    <button className="button" onClick={() => {
                                        setEstAffiche(false);
                                    }}>CANCEL
                                    </button>
                                    <button className="button is-danger" onClick={deleteItem}>DELETE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}

export {ItemAdmin};
