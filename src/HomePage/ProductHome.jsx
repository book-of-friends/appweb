import React, {useContext, useEffect, useState} from "react";
import "../css/HomePage.css";
import {Item} from "./Item";
import {Context} from "../App";
import {ItemAdmin} from "./ItemAdmin";
import {serveur} from "../CommonFunction/constantes.js";


function ProductHome() {
    const {admin} = useContext(Context);
    const [fetchAll, setFetchAll] = useState([]);
    const [filter, setFilter] = useState([]);
    const [typeProduct] = useState(window.location.pathname);
    const [message, setMessage] = useState("");
    const [input, setInput] = useState("");

    const handleChangeInput = (event) => setInput(event.target.value);

    async function getArticleByType() {
        try {
            const response = await fetch(`${serveur}${typeProduct}`);
            if (response.ok) {

                const fetchData = await response.json();
                setFetchAll(fetchData);
                setFilter(fetchData);
            } else if (response.status === 404) {
                const {message} = await response.json();
                setMessage(message);
            }
        } catch (error) {
            console.error("Error fetching articles:", error);
        }
    }

    useEffect(() => {
        getArticleByType().then(() => console.log("Articles fetched"));
    }, [typeProduct]);

    useEffect(() => {
        if (input.trim() !== "") {
            const filters = fetchAll.filter((item) => {
                const lowercaseSearch = input.toLowerCase();
                return (
                    item.title.toLowerCase().includes(lowercaseSearch) ||
                    item.author.toLowerCase().includes(lowercaseSearch) ||
                    item.type.toLowerCase().includes(lowercaseSearch) ||
                    item.language.toLowerCase().includes(lowercaseSearch)
                );
            });
            setFilter(filters);
        } else {
            setFilter(fetchAll);
        }
    }, [input]);

    const productTypeTitles = {
        "/products": "Available Works",
        "/albums": "Available Albums",
        "/mangas": "Available Mangas",
        "/romans": "Available Novels",
        "/movies": "Available Movies",
        default: "Available Books",
    };
    const productTypeTitle = productTypeTitles[typeProduct] || productTypeTitles.default;

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>{productTypeTitle}</h1>
                <br/>
                <div className="field is-horizontal is-centered">
                    <div className="field-body">
                        <div className="field">
                            <p className="control is-expanded">
                                <input
                                    id="titre"
                                    className="input"
                                    style={{width: "50%"}}
                                    type="text"
                                    size="sm"
                                    placeholder="Search for titles, authors, genres..."
                                    onChange={handleChangeInput}
                                />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <div>
                {message !== "" ? (
                    <div className="has-text-centered">
                        <br/>
                        <p className="title">{message}</p>
                    </div>
                ) : (
                    <>
                        {admin && filter.map((item) => <ItemAdmin item={item} key={item.id} admin={admin}/>)}
                        {!admin && filter.map((item) => <Item item={item} key={item.id}/>)}
                    </>
                )}
            </div>
        </main>
    );
}

export {ProductHome};
