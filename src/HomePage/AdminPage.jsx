import React from "react";
import {Link} from "react-router-dom";
import "../css/HomePage.css";

function AdminPage() {

    function renderLink(to, imgSrc, text, columnClass = "is-2 has-text-centered") {
        return (
            <Link to={to} className={`column ${columnClass} has-text-centered`}>
                <img src={imgSrc} alt={text}/>
                <p className="is-size-5-desktop is-size-4-mobile">{text}</p>
            </Link>
        );
    }

    return (
        <main className="container">
            <div className="has-text-centered">
                <h1>Bookstore Book of Friends</h1>
                <br/>
            </div>

            <div>
                <div className="columns is-mobile is-multiline is-centered">
                    {renderLink("/albums", "./cd.png", "Albums' list")}
                    {renderLink("/movies", "./clapperboard.png", "Movies' list")}
                    {renderLink("/books", "./list.png", "Books' list")}
                </div>

                <div className="columns is-mobile is-multiline is-centered">
                    {renderLink("/ajouter-album", "./music.png", "add album")}
                    {renderLink("/ajouter-film", "./add-video.png", "add movie")}
                    {renderLink("/ajouter-livre", "./book.png", "add book")}
                </div>

                <div className="columns is-mobile is-multiline is-centered">
                    {renderLink("/reservations", "./reserve.png", "Reservations")}
                    {renderLink("/commentaires", "./comments.png", "Comments")}
                </div>
            </div>
            <br/>
            <div></div>
        </main>
    );
}

export {AdminPage};
