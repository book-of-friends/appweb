import React from "react";

export function VerificationPass({password}) {
    return (
        <span id="passwordRequirements">
            {
                password.length >= 8 ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } At least 8 characters long. <br/>
            {
                /[A-Z]/.test(password) ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } Contains at least one uppercase letter. <br/>
            {
                /[a-z]/.test(password) ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } Contains at least one lowercase letter. <br/>
            {
                /\d/.test(password) ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } Contains at least one digit. <br/>
            {
                /[!@#$%^&*?]/.test(password) ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } Contains at least one special character (!@#$%^&*?). <br/>
        </span>
    );
}

export function VerificationConfirmation({password, confPassword}) {
    return (
        <span id="passwordConfirmation">
            {
                password === confPassword && password !== "" ?
                    <span className="icon is-small has-text-success">
                        <i className="fas fa-check"></i>
                    </span>
                    :
                    <span className="icon is-small has-text-danger">
                        <i className="fas fa-times"></i>
                    </span>
            } the password and its confirmation have to be similar. <br/>
        </span>
    );
}


export function VerificationISBN(props) {
    return (
        <div className="subtitle is-size-7">
            {
                (props.ISBN !== "" && /^(\d{13})?$/.test(props.ISBN)) ?
                    <p key="1" className="has-text-primary"> ✓ Contient 13 chiffres </p>
                    :
                    <p key="1" className="has-text-danger"> ✗ Contient 13 chiffres </p>
            }
        </div>
    );
}

export function VerificationEmail(props) {
    return (
        <div className="subtitle is-size-7">
            {
                (props.email !== "" && props.email.includes("@")) ?
                    <p key="1" className="has-text-primary"> ✓ Contient le caractére @ </p>
                    :
                    <p key="1" className="has-text-danger"> ✗ Contient le symbole @ </p>
            }
        </div>
    );
}

export function VerificationInput({input}) {
    const isValidInput = input !== "";
    return (
        <span id="passwordConfirmation">
            {isValidInput ? (
                <span className="icon is-small has-text-success">
                    <i className="fas fa-check"></i>
                </span>
            ) : (
                <span className="icon is-small has-text-danger">
                    <i className="fas fa-times"></i>
                </span>
            )}
            &nbsp;
            {isValidInput ? (
                "Required field filled."
            ) : (
                "Please fill in the required fields."
            )}
            <br/>
        </span>
    );
}

export const VerificationPhoneNumber = ({input}) => {
    const isValidPhoneNumber = /^\(\d{3}\) \d{3}-\d{4}$/.test(input);

    return (
        <span id="passwordConfirmation">
            <span className={`icon is-small has-text-${isValidPhoneNumber ? "success" : "danger"}`}>
                <i className={`fas fa-${isValidPhoneNumber ? "check" : "times"}`}></i>
            </span>
            &nbsp;
            {isValidPhoneNumber ? "Valid phone number." : "Please enter a valid phone number in the format (XXX) XXX-XXXX."}
            <br/>
        </span>
    );
};

export function VerificationCodePostal({codePostal}) {
    const isValidPostalCode = /^([A-Z]\d[A-Z])\s?(\d[A-Z]\d)$/.test(codePostal);

    return (
        <span id="passwordConfirmation">
            <span className={`icon is-small has-text-${isValidPostalCode ? "success" : "danger"}`}>
                <i className={`fas fa-${isValidPostalCode ? "check" : "times"}`}></i>
            </span>
            &nbsp;
            {isValidPostalCode ? (
                "Valid postal code."
            ) : (
                "Please enter a valid postal code in the format H1H 1H1."
            )}
            <br/>
        </span>
    );
}


