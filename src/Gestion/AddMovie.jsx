import React, {useEffect, useState} from "react";
import "../css/signup.css";
import {useParams} from "react-router-dom";
import Form from "./Form.jsx";
import Select from "react-select";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClose} from "@fortawesome/free-solid-svg-icons";
import {genreList, IS_MOVIE, serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {ErrorModel} from "../ErrorModel.jsx";
import {FileCoverPage} from "./FileCoverPage.jsx";
import {deleteArticle} from "../CommonFunction/ArticleManagment.js";
import {handleUpload} from "../CommonFunction/HandleUpload.js";
import {makeAlbumRequest} from "../CommonFunction/MakePostPutRequest.js";

function AddMovie() {

    const params = useParams();
    const isEditMode = params.idMovie;
    const [MovieCover, setMovieCover] = useState("");

    const {
        isErrorVisible,
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        error,
        showSuccess,
        showError
    } = useNotification();
    const [file, setFile] = useState();

    const [input, setInput] = useState("");
    const [movieData, setMovieData] = useState({
        title: "", rating: "", duration: "", seasons: "",
        country: "", releaseDate: "", synopsis: "", genres: [],
        language: "", actors: [], author: "", coverURL: "", type: ""
    });


    function handleChange(event) {
        const {name, value} = event.target;
        setMovieData({...movieData, [name]: value});
    }

    async function getMovie() {
        try {
            const response = await fetch(`${serveur}/movie?Id=${params.idMovie}`);
            const data = await response.json();

            if (response.ok) {
                setMovieCover(data.coverURL);
                setMovieData({...data});
            } else {
                showError(data.message);
            }
        } catch (err) {
            showError(err.message);
        }
    }

    useEffect(() => {
        if (params.idMovie) getMovie().then(() => console.log("Movie fetched"));
    }, [params.idMovie]);


    async function updateMovieInfo() {
        try {
            const coverId = MovieCover === movieData.coverURL ? movieData.coverURL : await handleUpload(file);

            const body = {
                title: movieData.title,
                synopsis: movieData.synopsis,
                rating: movieData.rating,
                coverURL: coverId,
                releaseDate: movieData.releaseDate,
                episodes: movieData.episodes,
                seasons: movieData.seasons,
                author: movieData.author,
                genres: movieData.genres,
                country: movieData.country,
                duration: movieData.duration,
                language: movieData.language,
                type: movieData.type,
                actors: movieData.actors,
            };

            await makeAlbumRequest(`${serveur}/movie?Id=${params.idMovie}`, "PUT", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }


    async function addMovie() {
        try {
            const coverId = await handleUpload(file);
            const body = {
                title: movieData.title,
                synopsis: movieData.synopsis,
                rating: movieData.rating,
                coverURL: coverId,
                releaseDate: movieData.releaseDate,
                episodes: movieData.episodes,
                seasons: movieData.seasons,
                author: movieData.author,
                genres: movieData.genres,
                country: movieData.country,
                duration: movieData.duration,
                language: movieData.language,
                type: movieData.type,
                actors: movieData.actors,
                movieState: "Available"
            };

            await makeAlbumRequest(`${serveur}/movie`, "POST", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }

    return (
        <main id="Content">
            <div className="form-control">
                <div className="has-text-centered">
                    {isEditMode ? <h1>Edit a movie/series!</h1> : <h1>Add a movie/series!</h1>}
                </div>

                <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                              setIsSuccessVisible={setIsSuccessVisible} page={"movies"}/>

                <fieldset>
                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="title">
                                <span>Title</span>
                            </label>
                            <input
                                type="text"
                                value={movieData.title}
                                name="title"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>

                        <div id="space">
                            <label htmlFor="synopsis">
                                <span>Description</span>
                            </label>
                            <textarea
                                type="text"
                                id="synopsis"
                                value={movieData.synopsis}
                                rows="8"
                                name="synopsis"
                                onChange={handleChange}
                                required
                            ></textarea>
                        </div>

                        <div id="space">
                            <label htmlFor="genres">
                                <span>Genres</span>
                            </label>
                            <Select
                                id="select"
                                placeholder="Choose a genre..."
                                options={genreList}
                                value={movieData.genres}
                                onChange={handleChange}
                                isSearchable={true}
                                isMulti
                            />
                        </div>

                        <div id="space">
                            <label htmlFor="author">
                                <span>Author</span>
                            </label>
                            <input
                                type="text"
                                id="author"
                                name="author"
                                value={movieData.author}
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>

                        <div id="space">
                            <label htmlFor="country">
                                <span>Country of Origin</span>
                            </label>
                            <input
                                type="text"
                                id="country"
                                name="country"
                                value={movieData.country}
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>

                    <div className="fieldset-wrapper columns is-mobile is-multiline">
                        <div id="space" className="column">
                            <label htmlFor="movieState">
                                <span>Language</span>
                            </label>
                            <input
                                type="text"
                                list="langues"
                                value={movieData.language}
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                            <datalist id="langues">
                                <option value="French">French</option>
                                <option value="English">English</option>
                                <option value="Japanese">Japanese</option>
                                <option value="Spanish">Spanish</option>
                                <option value="Arabic">Arabic</option>
                            </datalist>
                        </div>
                        <div id="space" className="column">
                            <label htmlFor="typeMovie">
                                <span>Movie Type</span>
                            </label>
                            <select
                                id="typeMovie"
                                value={movieData.type}
                                className="select form-control"
                                onChange={handleChange}
                            >
                                <option></option>
                                <option value="None">None</option>
                                <option value="Series">Series</option>
                                <option value="Film">Film</option>
                                <option value="Documentary">Documentary</option>
                            </select>
                        </div>
                    </div>

                    <div className="fieldset-wrapper columns is-mobile is-multiline">
                        <div id="space" className="column">
                            <label htmlFor="seasons">
                                <span>Number of Seasons</span>
                            </label>
                            <input
                                type="number"
                                min="0"
                                id="seasons"
                                value={movieData.seasons}
                                name="seasons"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>

                        <div id="space" className="column">
                            <label htmlFor="duration">
                                {(movieData.type === "Series") ? (
                                    <span>Number of Episodes</span>
                                ) : (movieData.type === "Documentary") ? (
                                    <span>Number of Episodes or Duration</span>
                                ) : (
                                    <span>Duration</span>
                                )}
                            </label>
                            <input
                                type="text"
                                id="duration"
                                name="duration"
                                value={movieData.duration}
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>


                    <div className="fieldset-wrapper columns is-mobile is-multiline">
                        <div id="space" className="column">
                            <label htmlFor="rating">
                                <span>Rating</span>
                            </label>
                            <input
                                type="number"
                                min="0"
                                max="10"
                                step="0.01"
                                value={movieData.rating}
                                id="rating"
                                name="rating"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>

                        <div id="space" className="column">
                            <label htmlFor="datePub">
                                <span>Publication Date</span>
                            </label>
                            <input
                                min="1963-01-01"
                                data-min-year="1963"
                                value={movieData.releaseDate}
                                max="2024-01-31"
                                data-max-year="2023"
                                type="date"
                                data-drupal-date-format="Y-m-d"
                                data-drupal-selector="datePub"
                                id="datePub"
                                name="datePub"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>

                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="actors">
                                <span>Actors</span>
                            </label>
                        </div>

                        <Form
                            input={input}
                            setInput={setInput}
                            formData={movieData}
                            setFormData={setMovieData}
                        />

                        <div className="columns is-mobile is-multiline">
                            {movieData.actors.map((actor) => (
                                <li key={actor.name} className="column">
                                    <input
                                        type="text"
                                        value={actor.name}
                                        onChange={(event) => event.preventDefault()}
                                        disabled
                                    />
                                    &nbsp;&nbsp;&nbsp;
                                    <FontAwesomeIcon
                                        icon={faClose}
                                        onClick={() => {
                                            const index = movieData.actors.indexOf(actor);
                                            if (index > -1) {
                                                const updatedActors = [...movieData.actors];
                                                updatedActors.splice(index, 1);
                                                setMovieData({...movieData, actors: updatedActors});
                                            }
                                        }}
                                    />
                                </li>
                            ))}
                        </div>
                        <br/>
                    </div>

                    <FileCoverPage filename={movieData.coverURL} setFile={setFile}/>
                </fieldset>
                <br/>

                <ErrorModel error={error} isErrorVisible={isErrorVisible}/>
                <br/>
                <div className="has-text-centered">
                    <input
                        className="button submit"
                        onClick={async () => isEditMode ? await updateMovieInfo() : await addMovie()}
                        value={isEditMode ? "Edit" : "Add"}
                        style={{cursor: "pointer"}}
                    />
                    {isEditMode && (
                        <input
                            className="button submit is-danger"
                            onClick={async () => {
                                await deleteArticle(showError, showSuccess, IS_MOVIE, params.idMovie);
                            }}
                            value="Delete"
                            style={{cursor: "pointer"}}
                        />
                    )}
                </div>
                <br/>
            </div>
            <br/>
        </main>
    );
}

export {AddMovie};
