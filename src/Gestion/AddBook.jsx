import React, {useEffect, useState} from "react";
import "../css/signup.css";
import Select from "react-select";
import {useParams} from "react-router-dom";
import {genreList, IS_BOOK, serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {ErrorModel} from "../ErrorModel.jsx";
import {FileCoverPage} from "./FileCoverPage.jsx";
import {deleteArticle} from "../CommonFunction/ArticleManagment.js";
import {handleUpload} from "../CommonFunction/HandleUpload.js";
import {makeAlbumRequest} from "../CommonFunction/MakePostPutRequest.js";

function AddBook() {
    const params = useParams();
    const isEdit = params.idBook;

    const {
        isErrorVisible,
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        error,
        showSuccess,
        showError
    } = useNotification();

    const [BookCover, setBookCover] = useState("");
    const [file, setFile] = useState();
    const [bookData, setBookData] = useState({
        title: "", ISBN: "", type: "", releaseDate: "",
        author: "", coverURL: "", language: "", nbrPages: 0,
        plot: "", genres: [],
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setBookData({...bookData, [name]: value});
    }

    async function getBook({ISBN}) {
        try {
            const response = await fetch(`${serveur}/book?Id=${ISBN}`);
            const data = await response.json();

            if (response.ok) {
                setBookCover(data.coverURL);
                setBookData({...data});
            } else {
                showError(data.message);
            }
        } catch (err) {
            console.error(err.message);
        }
    }

    useEffect(() => {
        if (params.idBook) getBook({ISBN: params.idBook}).then(() => console.log("Book fetched"));
    }, [params.idBook]);


    async function updateBookInfo() {
        try {
            const coverId = BookCover === bookData.coverURL ? bookData.coverURL : await handleUpload(file);

            const body = {
                ISBN: bookData.ISBN,
                title: bookData.title,
                plot: bookData.plot,
                author: bookData.author,
                nbrPages: bookData.nbrPages,
                language: bookData.language,
                coverURL: coverId,
                releaseDate: bookData.releaseDate,
                type: bookData.type,
                genres: bookData.genres,
            };

            await makeAlbumRequest(`${serveur}/book?Id=${params.idBook}`, "PUT", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }


    async function addBook() {
        try {
            const coverId = await handleUpload(file);
            const body = {
                ISBN: bookData.ISBN,
                title: bookData.title,
                plot: bookData.plot,
                author: bookData.author,
                nbrPages: bookData.nbrPages,
                language: bookData.language,
                coverURL: coverId,
                releaseDate: bookData.releaseDate,
                type: bookData.type,
                genres: bookData.genres,
                bookState: "Available",
            };

            await makeAlbumRequest(`${serveur}/book`, "POST", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }


    return (
        <main id="Contenu">
            <div className="form-control">
                <div className="has-text-centered">
                    {isEdit ? <h1>Edit a book!</h1> : <h1>Add a book!</h1>}
                </div>

                <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                              setIsSuccessVisible={setIsSuccessVisible} page={"books"}/>

                <fieldset>
                    <div className="fieldset-wrapper">
                        <div id="space">
                            <label htmlFor="title">
                                <span>Title</span>
                            </label>
                            <input
                                type="text"
                                value={bookData.title}
                                name="title"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="ISBN">
                                <span>ISBN</span>
                            </label>
                            <input
                                type="text"
                                id="ISBN"
                                value={bookData.ISBN}
                                name="ISBN"
                                maxLength="13"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="plot">
                                <span>Description</span>
                            </label>
                            <textarea
                                type="text"
                                id="plot"
                                value={bookData.plot}
                                rows="8"
                                name="plot"
                                onChange={handleChange}
                                required
                            ></textarea>
                        </div>
                        <div id="space">
                            <label htmlFor="genres">
                                <span>Genres</span>
                            </label>
                            <Select
                                id="select"
                                placeholder="Choose a genre..."
                                options={genreList}
                                value={bookData.genres}
                                onChange={handleChange}
                                isSearchable={true}
                                isMulti
                            />
                        </div>
                        <div id="space">
                            <label htmlFor="auteur">
                                <span>Author</span>
                            </label>
                            <input
                                type="text"
                                id="auteur"
                                value={bookData.author}
                                name="auteur"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="fieldset-wrapper columns is-mobile">
                        <div id="space" className="column">
                            <label htmlFor="typeBook">
                                <span>Book Type</span>
                            </label>
                            <select
                                id="typeBook"
                                value={bookData.type}
                                className="select form-control"
                                onChange={handleChange}
                            >
                                <option></option>
                                <option value="Manga">Manga</option>
                                <option value="Novel">Novel</option>
                            </select>
                        </div>

                        <div id="space" className="column">
                            <label htmlFor="language">
                                <span>Language</span>
                            </label>
                            <input
                                type="text"
                                list="languages"
                                value={bookData.language}
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                            <datalist id="languages">
                                <option value="French">French</option>
                                <option value="English">English</option>
                                <option value="Japanese">Japanese</option>
                                <option value="Spanish">Spanish</option>
                                <option value="Arabic">Arabic</option>
                            </datalist>
                        </div>
                    </div>


                    <div className="fieldset-wrapper columns is-mobile">
                        <div className="column" id="space">
                            <label htmlFor="datePub">
                                <span>Publication Date</span>
                            </label>
                            <input
                                min="1963-01-01"
                                value={bookData.releaseDate}
                                data-min-year="1963"
                                max="2024-01-31"
                                data-max-year="2023"
                                type="date"
                                data-drupal-date-format="Y-m-d"
                                data-drupal-selector="datePub"
                                id="datePub"
                                name="datePub"
                                className="form-control"
                                onChange={handleChange}
                                required
                            />
                        </div>
                        {bookData.type === "Manga" ? (
                            <div id="space" className="column">
                                <label htmlFor="nbrPage">
                                    <span>Number of Volumes</span>
                                </label>
                                <input
                                    className="form-control"
                                    value={bookData.nbrPages}
                                    type="number"
                                    min="0"
                                    id="nbrPage"
                                    name="nbrPage"
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                        ) : (
                            <div id="space" className="column">
                                <label htmlFor="nbrPage">
                                    <span>Number of Pages</span>
                                </label>
                                <input
                                    className="form-control"
                                    type="number"
                                    min="0"
                                    id="nbrPage"
                                    name="nbrPage"
                                    onChange={handleChange}
                                    required
                                />
                            </div>
                        )}
                    </div>

                    <FileCoverPage filename={bookData.coverURL} setFile={setFile}/>
                </fieldset>

                <br/>

                <ErrorModel error={error} isErrorVisible={isErrorVisible}/>
                <br/>
                <div className="has-text-centered">
                    <input
                        className="button submit"
                        onClick={async () => isEdit ? await updateBookInfo() : await addBook()}
                        value={isEdit ? "Edit" : "Add"}
                        style={{cursor: "pointer"}}
                    />
                    {isEdit && (
                        <input
                            className="button submit is-danger"
                            onClick={async () => {
                                await deleteArticle(showError, showSuccess, IS_BOOK, params.idBook);
                            }}
                            value="Delete"
                            style={{cursor: "pointer"}}
                        />
                    )}
                </div>
                <br/>
            </div>
            <br/>
        </main>
    );
}

export {AddBook};
