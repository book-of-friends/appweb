import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClose} from "@fortawesome/free-solid-svg-icons";
import Form from "./Form.jsx";
import "../css/signup.css";
import {IS_ALBUM, serveur} from "../CommonFunction/constantes.js";
import {useNotification} from "../CommonFunction/CommunFunction.js";
import {SuccessModel} from "../SuccessModel.jsx";
import {ErrorModel} from "../ErrorModel.jsx";
import {FileCoverPage} from "./FileCoverPage.jsx";
import {deleteArticle} from "../CommonFunction/ArticleManagment.js";
import {makeAlbumRequest} from "../CommonFunction/MakePostPutRequest.js";
import {handleUpload} from "../CommonFunction/HandleUpload.js";

function AddAlbum() {
    const params = useParams();
    const isEditMode = params.idAlbum;

    const {
        isErrorVisible,
        isSuccessVisible,
        setIsSuccessVisible,
        success,
        error,
        showSuccess,
        showError
    } = useNotification();

    const [AlbumCover, setAlbumCover] = useState("");
    const [file, setFile] = useState(null);
    const [input, setInput] = useState("");
    const [albumData, setAlbumData] = useState({
        title: "", label: "", releaseDate: "", author: "",
        language: "", tracks: [], coverURL: "",
    });

    useEffect(() => {
        if (params.idAlbum) getAlbum(params.idAlbum).then(() => console.log("Album fetched"));
    }, [params.idAlbum]);


    function handleChange(event) {
        const {name, value} = event.target;
        setAlbumData({...albumData, [name]: value});
    }

    async function getAlbum(id) {
        try {
            const response = await fetch(`${serveur}/album?Id=${id}`);
            const data = await response.json();

            if (response.ok) {
                setAlbumCover(data.coverURL);
                setAlbumData({...data});
            } else {
                showError(data.message);
            }
        } catch (err) {
            showError(err.message);
        }
    }


    async function updateAlbumInfo() {
        try {
            const coverId = AlbumCover === albumData.coverURL ? albumData.coverURL : await handleUpload(file);

            const body = {
                title: albumData.title,
                label: albumData.label,
                releaseDate: albumData.releaseDate,
                author: albumData.author,
                language: albumData.language,
                tracks: albumData.tracks,
                coverURL: coverId,
            };

            await makeAlbumRequest(`${serveur}/album?Id=${params.idAlbum}`, "PUT", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }


    async function addAlbum() {
        try {
            const coverId = await handleUpload(file);
            const body = {
                title: albumData.title,
                label: albumData.label,
                releaseDate: albumData.releaseDate,
                author: albumData.author,
                language: albumData.language,
                tracks: albumData.tracks,
                coverURL: coverId,
                albumState: "Available",
            };

            await makeAlbumRequest(`${serveur}/album`, "POST", body, showSuccess, showError);

        } catch (error) {
            console.error(error);
            showError("An unexpected error occurred.");
        }
    }


    return (
        albumData && (
            <main id="Contenu">
                <div className="form-control">
                    <div className="has-text-centered">
                        {isEditMode ? <h1>Edit album!</h1> : <h1>Add an album!</h1>}
                    </div>

                    <SuccessModel success={success} isSuccessVisible={isSuccessVisible}
                                  setIsSuccessVisible={setIsSuccessVisible} page={"albums"}/>

                    <fieldset>
                        <div className="fieldset-wrapper">
                            <div id="space">
                                <label htmlFor="title">
                                    <span>Title</span>
                                </label>
                                <input type="text" value={albumData.title} name="title" className="form-control"
                                       onChange={handleChange} required/>
                            </div>
                            <div id="space">
                                <label htmlFor="author">
                                    <span>Author</span>
                                </label>
                                <input type="text" id="author" value={albumData.author} name="author"
                                       className="form-control" onChange={handleChange} required/>
                            </div>
                            <div id="space">
                                <label htmlFor="label">
                                    <span>Label</span>
                                </label>
                                <input type="text" id="label" name="label" value={albumData.label}
                                       className="form-control" onChange={handleChange} required/>
                            </div>
                        </div>

                        <div className="fieldset-wrapper columns is-mobile">
                            <div id="space" className="column">
                                <label htmlFor="language">
                                    <span>Language</span>
                                </label>
                                <input type="text" name="language" list="languages" className="form-control"
                                       value={albumData.language} onChange={handleChange} required/>
                                <datalist id="languages">
                                    <option value="French">French</option>
                                    <option value="Korean">Korean</option>
                                    <option value="English">English</option>
                                    <option value="Japanese">Japanese</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="Arabic">Arabic</option>
                                </datalist>
                            </div>
                            <div id="space" className="column">
                                <label htmlFor="releaseDate">
                                    <span>Release Date</span>
                                </label>
                                <input min="1963-01-01" data-min-year="1963" max="2024-01-31" data-max-year="2023"
                                       type="date" data-drupal-date-format="Y-m-d"
                                       data-drupal-selector="releaseDate" id="releaseDate" name="releaseDate"
                                       value={albumData.releaseDate} className="form-control" onChange={handleChange}
                                       required/>
                            </div>
                        </div>

                        <div className="fieldset-wrapper">
                            <div id="space">
                                <label htmlFor="tracks">
                                    <span>Tracks</span>
                                </label>
                            </div>

                            <Form
                                input={input} setInput={setInput}
                                formData={albumData} setFormData={setAlbumData}
                            />

                            <div className="columns is-mobile is-multiline">
                                {
                                    albumData.tracks.map((track) => (
                                        <li key={track.name} className="column">
                                            <input type="text"
                                                   value={track.name}
                                                   onChange={(event) => event.preventDefault()} disabled/>
                                            &nbsp;&nbsp;&nbsp;
                                            <FontAwesomeIcon icon={faClose} onClick={() => {

                                                const index = albumData.tracks.indexOf(track);
                                                if (index > -1) {
                                                    const updatedTracks = [...albumData.tracks];
                                                    updatedTracks.splice(index, 1);
                                                    setAlbumData({...albumData, tracks: updatedTracks});
                                                }
                                            }}/>
                                        </li>
                                    ))
                                }
                            </div>
                            <br/>
                        </div>

                        <FileCoverPage filename={albumData.coverURL} setFile={setFile}/>
                    </fieldset>

                    <br/>

                    <ErrorModel error={error} isErrorVisible={isErrorVisible}/>
                    <br/>
                    <div className="has-text-centered">
                        <input
                            className="button submit"
                            onClick={async () => isEditMode ? await updateAlbumInfo() : await addAlbum()}
                            value={isEditMode ? "Edit" : "Add"}
                            style={{cursor: "pointer"}}
                        />
                        {isEditMode && (
                            <input
                                className="button submit is-danger"
                                onClick={async () => {
                                    await deleteArticle(showError, showSuccess, IS_ALBUM, params.idAlbum);
                                }}
                                value="Delete"
                                style={{cursor: "pointer"}}
                            />
                        )}
                    </div>
                    <br/>
                </div>
                <br/>
            </main>
        )
    );
}

export {AddAlbum};
