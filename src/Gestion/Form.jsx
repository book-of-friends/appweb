import React from "react";

const Form = ({input, setInput, formData, setFormData}) => {
    const onInputChange = (e) => {
        setInput(e.target.value);
    };

    const isMovie = formData.tracks === undefined;
    const inputId = isMovie ? "actors" : "tracks";
    const buttonId = isMovie ? "actorAdd" : "trackAdd";

    const onFormSubmit = (e) => {
        e.preventDefault();

        const updatedFormData = isMovie
            ? {...formData, actors: [...formData.actors, {name: input}]}
            : {...formData, tracks: [...formData.tracks, {name: input}]};

        setFormData(updatedFormData);
        setInput("");
    };

    return (
        <form onSubmit={onFormSubmit}>
            <div className="columns form-control is-mobile is-multiline">
                <input
                    type="text"
                    id={inputId}
                    name={inputId}
                    className="column is-10-desktop is-9-mobile form-control"
                    value={input}
                    required
                    onChange={onInputChange}
                />
                <button id={buttonId} className="column form-control" type="submit">
                    Add
                </button>
            </div>
        </form>
    );
};

export default Form;
