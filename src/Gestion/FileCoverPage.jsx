import React, {useState} from "react";
import "../css/signup.css";
import {serveur} from "../CommonFunction/constantes.js";

function FileCoverPage({filename, setFile}) {

    const [previewUrl, setPreviewUrl] = useState(null);

    const handleFileChange = (e) => {
        const selectedFile = e.target.files[0];

        if (selectedFile) {
            setFile(selectedFile);
            setPreviewUrl(URL.createObjectURL(selectedFile));
        }
    };

    return (
        <div className="fieldset-wrapper">
            <div id="space">
                <label htmlFor="coverPage" style={{cursor: "pointer"}}>
                    <span>Cover Page</span>
                </label>
                <input
                    type="file"
                    className="form-control"
                    accept="image/*"
                    name="image"
                    id="coverPage"
                    onChange={handleFileChange}
                    required
                />
                <br/>
                <div className="has-text-centered">
                    <img
                        id="coverPreview"
                        alt="coverUrl"
                        src={
                            previewUrl ||
                            (filename &&
                                `${serveur}/cover/${filename}`) ||
                            "./image.png"
                        }
                        width="300"
                        style={{cursor: "pointer"}}
                    />
                </div>
            </div>
        </div>
    );
}

export {FileCoverPage};
