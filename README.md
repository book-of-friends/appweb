# Book Of Friends - Client (Frontend - AppWeb) : EN COURS

## Description

Dans le cadre du cours de projet intégrateur, J'ai développé une application web destiné pour une petite librairie
fictif qui vend des livres, bandes dessinées/mangas, films et albums. L’application permettra au client (utilisateur)
d’avoir une liste d’œuvre et de réserver un article et voir si celui-ci est disponible en magasin ou pas. Les
utilisateurs doivent être connectés pour réserver une œuvre ou s’inscrire s’ils n’ont pas de compte. Elle permettra
aussi à l'administrateur de consulter, modifier, ajouter et supprimer un document.

Pour développer le coté client soit l'interface user de l'application, j'ai utilisé le Framework ReactJS pour développer
l'intégralité du frontend avec le loanguage de programmation JavaScript. Plusisuers pages ont déjà été intégré comme la
page de connexion, l'inscription, etc. Le coté client offre deux aspects de l'application: un user/abonné et un
administrateur la page d'accueil differe entre les deux.

le coyé frontend nous permet d'envoyer des requetes verss l'API pour GET des données, POST, PUT ou DELETE.

Les images ajoutées par l'administrateur lors de l'ajout d'un document sont ajouté directement dans le storage de
firebase du client.

Pour avoir une application logique et réaliste j'ai pris comme exemple l'application web de la grande bibliotheque de
montréal https://cap.banq.qc.ca/accueil.
Crédit: Le CSS de la page de connexion utilisé est le meme que celui de la grande bibliotheque.

## Installation

pour pouvoir utiliser l'application web veuillez suivre les instructions:

### 1- Faire un clone du projet localement

lancez la commande git clone https://gitlab.com/book-of-friends/appweb.git dans cotre terminal

### 2- Placez vous dans le bon répertoire soit AppWeb

### 3- veuillez ensuite installer les dépendances nécessaires

npm install ou npm i

### 4- Lancez la commande suivante dans le terminal pour lancer le client

npm start

### 5- Pour accéder à l'application, appuyer sur le lien

http://localhost:5173 si tous ce produit bien vous serez diriger vers la page d'accueil

## Support

En cas de problemess ou de difficultées contactez samylichine.iss@gmail.com

## contributions

Ce projet est développé par: Samy Issiakhem

